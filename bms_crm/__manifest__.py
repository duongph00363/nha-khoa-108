# -*- coding: utf-8 -*-

{
    'name': 'BMS CRM - Sale Improvement',
    'summary': 'BMS CRM - Sale Improvement',
    'version': '1.0.0',
    'author': 'BMS',
    'category': 'BMS Application',
    'license': 'AGPL-3',
    'website': 'http://www.bmstech.io',
    'sequence': 1,
    'depends': ['base','crm'],
    'data': [
        'views/bms_crm_phone_search.xml',
    ],
    'css': [],
    'qweb': [],
    'installable': True,
    'application': True,
    'auto_install': False,
    'description': """
		Improve the the easy use of BMS CRM
	""",
}
