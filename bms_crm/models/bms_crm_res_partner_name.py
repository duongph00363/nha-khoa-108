# -*- coding: utf-8 -*-

from odoo import fields, models, api


class ResPartner(models.Model):
    _inherit = 'res.partner'

    @api.multi
    @api.depends('name', 'phone')
    def name_get(self):
        result = []

        for post in self:
            result.append((post.id, '%s - %s' % (post.name, post.phone)))
        return result

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        domain = []
        if name:
            domain = ['|', ('name', operator, name), ('phone', operator, name)]
        picks = self.search(domain + args, limit=limit)
        return picks.name_get()
