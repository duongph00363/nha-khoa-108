# -*- coding: utf-8 -*-
{
    'name': "Quản lý phòng khám",

    'summary': """
        Module quản lý phòng khám của BMS TECHNOLOGY""",

    'description': """
        Module quản lý phòng khám của BMS TECHNOLOGY
    """,

    'author': "BMS TECHNOLOGY",
    'website': "http://www.bmstech.io",
    # support for show when filter App
    'installable': True,
    'application': True,
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'BMS Clinic',
    'version': '0.1',

    # any module necessary for this one to work correctly

    'depends': ['base', 'account', 'hr', 'crm', 'sale_management', 'product', 'base_setup', 'product', 'analytic',
                'web_planner', 'mail', 'hr_holidays', 'hr_payroll',
                'portal', 'website', 'base_vat_autocomplete', 'calendar'],

    # always loaded
    'data': [
        # 'views/import_template.xml',
        'security/bms_clinic_server_group.xml',
        'security/bms_clinic_record_rule.xml',
        'security/ir.model.access.csv',
        'views/bms_clinic_product_category.xml',
        'views/bms_Clinic_Ir_Sequence_view.xml',
        'report/bms_Clinic_Report_Account_Payment.xml',
        'views/bms_Clinic_Use_view.xml',
        'views/bms_Clinic_coupon.xml',
        'views/bms_Clinic_Result_Demo_view.xml',
        'views/bms_Clinic_AccountInvoice_view.xml',
        'views/bms_Clinic_Employees_view.xml',
        'views/bms_Clinic_Remedy_view.xml',
        'views/bms_Clinic_UnitCategory_view.xml',
        'views/bms_Clinic_Product_Sevices_view.xml',
        'views/bms_Clinic_Product_Drug_view.xml',
        'views/bms_Clinic_ResourceRefCustom_view.xml',
        'views/bms_Clinic_Advice_view.xml',
        'views/bms_Clinic_Preprocess_view.xml',
        'report/bms_Clinic_Report_primaryInfo.xml',
        'report/bms_Clinic_Report_patient.xml',
        'report/bms_Clinic_Report_ResultCLS.xml',
        'report/bms_Clinic_Report_Monocuminformation.xml',
        'report/bms_Clinic_Report_Payflast.xml',
        'views/bms_drug.xml',
        'views/bms_Clinic_sale_order_line_view.xml',
        'views/bms_group_unit.xml',
        'views/bms_service.xml',
        'views/bms_unit.xml',
        'views/bms_view_country_group.xml',
        'views/bms_Clinic_Report.xml',
        'views/bms_Clinic_MonocumInformation_view.xml',
        'views/bms_Clinic_CLSresults_view.xml',
        'views/bms_Clinic_SampleMedicine_view.xml',
        'views/bms_Clinic_CustominInvoices_view.xml',
        'views/bms_res_partner.xml',
        # 'views/report_price_paid.xml',
        'report/bms_Clinic_Report_info_payment.xml',
        'report/bms_Clinic_Report_specifiedSevices_employee.xml',
        'views/bms_Clinic_service_assign.xml',
        # 'views/bms_Clinic_report_payment.xml',
        'report/bms_Clinic_Report_address.xml',
        'report/bms_Clinic_Report_age.xml',
        'report/bms_Clinic_Report_payment_total.xml',
        'views/bms_Clinic_report_loan.xml',
        'report/bms_Clinic_revenue_Doctors.xml',
        'views/bms_clinic_calendar.xml',
        'views/bms_clinic_report_primaryinformation.xml',
        'views/bms_Clinic_PrimaryInformation_view.xml',
        'data/data_product_uom.xml',
        'views/report_sale_order_line_view.xml',
        'views/report_sale_order_line_customer_view.xml',
        'views/bms_Clinic_Acount_Payment_view.xml',
        'views/bms_clinic_report_res_partner.xml',
        'views/bms_Clinic_report_respartner.xml',
        'views/menu.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        # 'demo/demo.xml',
    ],
    'css': [],
    # 'css': ['static\css\mycss.css'],
    # 'qweb': ['static/src/xml/template.xml'],
    'qweb': [],
}
