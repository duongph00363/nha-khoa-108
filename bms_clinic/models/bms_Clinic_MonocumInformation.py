#khanh.pg add 08.07.2018
# -*- coding: utf-8 -*-

from odoo import fields, models,api
from datetime import datetime

#khanh.pg add 08.07.2018
class BmsClinicUsed(models.Model):
    _name = "bms.clinic.use"
    _description = 'chi tiết cách dung'
    _order = "id desc"

    NameOfMedicine = fields.Many2one('product.template', string='Tên của thuốc')
    amount = fields.Float(string='Số lượng')
    used = fields.Many2one('bms.clinic.advice',string='Cách dùng')
    use_id = fields.Many2one(comodel_name='bms.clinic.monocuminformation')
    samplemedicine_id = fields.Many2one(comodel_name='bms.clinic.samplemedicine')
    company_id = fields.Many2one('res.company', string="Công ty", default=lambda self: self.env.user.company_id)
    monocuminformation_id = fields.Many2one('bms.clinic.monocuminformation')


class BmsClinicMonocumInformation(models.Model):
    _name = "bms.clinic.monocuminformation"
    _description = "Kê đơn"

    customer = fields.Many2one('res.partner', string='Khách hàng')
    code = fields.Char('Mã đơn thuốc', compute='_compute_code')
    dateStart = fields.Datetime(string='Ngày kê đơn', default=datetime.today())
    doctorPrescribed = fields.Many2one('hr.employee', string='Bác sĩ kê đơn',default=lambda self: self.sudo().env.uid)

    samplemedicine_id = fields.Many2one('bms.clinic.samplemedicine', string='Đơn thuốc mẫu')
    prescriptionName = fields.Char(string='Tên đơn thuốc')
    advice = fields.Many2one('bms.clinic.advice', string='Lời dặn')
    use_ids = fields.One2many('bms.clinic.use', 'monocuminformation_id',string='Chi tiết đơn thuốc')
    monocum_id = fields.Many2one(comodel_name='bms.clinic.primaryinformation')
    reasonTo1 = fields.Char(string='Lý do đến khám')

    active = fields.Boolean('Trạng thái', default=True)
    company_id = fields.Many2one('res.company', string="Công ty", default=lambda self: self.env.user.company_id)

    @api.multi
    @api.onchange('samplemedicine_id')
    def onchange_samplemedicine_id(self):
        self.prescriptionName = self.samplemedicine_id.name
        self.advice= self.samplemedicine_id.advice_id
        self.use_ids = self.samplemedicine_id.prescription_details_ids


    @api.multi
    def name_get(self):
        res = []
        for record in self:
            name = record.customer.name
            res.append((record.id, name))
        return res


    @api.multi
    def _compute_code(self):
        for item in self:
            item.code = 'DT' + str(item.id)