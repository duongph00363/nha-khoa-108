# -*- coding: utf-8 -*-

from odoo import fields, models, api

# kết quả mẫu
class BmsResultDemo(models.Model):
    _name = 'bms.clinic.result.demo'
    _description = 'Kết quả mẫu'
    _order = "id desc"

    name = fields.Char(string='Tên kết quả mẫu', required=True)
    code_result_demo = fields.Char(string='Mã kết quả mẫu', readonly=True,compute='_compute_code_result_demo')
    notes = fields.Text(string='Ghi chú')
    description_result = fields.Html(string='Nội dung kết quả', required=True)
    comment = fields.Char(string='Kết luận', required=True)
    active = fields.Boolean('Trạng thái', default=True)
    company_id = fields.Many2one('res.company', string="Công ty", default=lambda self: self.env.user.company_id)

    @api.multi
    def _compute_code_result_demo(self):
        for item in self:
            item.code_result_demo = 'KQM' + str(item.id)
