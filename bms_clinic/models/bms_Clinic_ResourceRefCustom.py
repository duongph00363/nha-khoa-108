# -*- coding: utf-8 -*-

from odoo import fields, models, api

class BmsClinicResourceRefCustom(models.Model):
    _name = 'bms.clinic.resourcerefcustom'
    _description = 'Nguồn khách hàng'
    _order = "id desc"

    client_code = fields.Char(string='Mã', readonly=True, compute='_compute_client_code')
    name = fields.Char(string='Tên nguồn khách', required=True)
    description = fields.Text(string='Mô tả')
    active = fields.Boolean('Trạng thái',default=True)
    company_id = fields.Many2one('res.company', string="Công ty", default=lambda self: self.env.user.company_id)

    @api.multi
    def _compute_client_code(self):
        for item in self:
            item.client_code = 'KH' + str(item.id)
