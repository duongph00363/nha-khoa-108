# -*- coding: utf-8 -*-

from odoo import fields, models,api
from odoo.exceptions import Warning
import re

#khanh.pg 08.07.2018 sản phẩm
class BmsClinicProductDrug(models.Model):
    _inherit = 'product.template'
    _order = "id desc"

    _sql_constraints = [
        ('default_code_unique', 'UNIQUE (default_code)', 'Mã thuốc đã bị trùng')
    ]

    @api.multi
    @api.onchange("default_code")
    def _check_default_code(self):
        if self.default_code:
            if len(self.default_code) >= 25:
                raise Warning('Mã Thuốc phải nhỏ hơn 25 ký tự')
            else:
                if not re.match("^[a-zA-Z0-9_]*$", self.default_code):
                    raise Warning('Mã Thuốc không được chứa ký tự đặc biệt')

    @api.multi
    @api.onchange("categ_id")
    def _check_categ_id(self):
        if len(self.categ_id) >= 255:
            if len(self.categ_id) >= 255:
                raise Warning('nhóm sản phẩm không được lớn hơn 255 ký tự')