# -*- coding: utf-8 -*-

from odoo import fields, models, api
from datetime import date


# khanh.pg 08.07.2018 trả kết quả cận lâm sàng
class BmsCLSresults(models.Model):
    _name = 'bms.clinic.clsresult'
    _description = 'trả kết quả cận lâm sàng'

    name = fields.Many2one('res.partner', string="Khách hàng")
    dateImplementation = fields.Datetime(string='Ngày thực hiện', default=date.today())
    doctorsDo = fields.Many2one(comodel_name='hr.employee', string="Bác sĩ thực hiện",default=lambda self: self.sudo().env.uid)
    sevices_id = fields.Many2one('product.template', string='Dịch vụ')

    sample_results = fields.Many2one('bms.clinic.result.demo', string='Kết quả mẫu')
    result = fields.Html(string='Kết quả')
    comment = fields.Char(string='Kết luận')
    reason_to = fields.Char(string='Lý do đến khám')
    image = fields.Char(string='Đường dẫn', default='Đường dẫn tĩnh')
    image_CLS1 = fields.Binary(string='Hình ảnh 1')
    image_CLS2 = fields.Binary(string='Hình ảnh 2')
    image_CLS3 = fields.Binary(string='Hình ảnh 3')
    image_CLS4 = fields.Binary(string='Hình ảnh 4')
    description = fields.Text(string='Mô tả')
    status = fields.Selection([(1, 'Hoàn tất'), (2, 'Đang chờ')], string='Trạng thái')
    primaryInformation_id = fields.Many2one('bms.clinic.primaryinformation')
    company_id = fields.Many2one('res.company', string="Công ty", default=lambda self: self.env.user.company_id)


    @api.multi
    @api.onchange('sample_results')
    def onchange_sample_results(self):
        self.result = self.sample_results.description_result
        self.comment =self.sample_results.comment


