# -*- coding: utf-8 -*-

from odoo import fields, models, api
import time
from unidecode import unidecode
from odoo.exceptions import UserError
from datetime import datetime, timedelta
import math

class bms_Clinic_report_customer(models.Model):
    _name = 'bms.clinic.report.res.partner'

    date_start = fields.Date("Ngày bắt đầu")
    date_end = fields.Date("Ngày kết thúc")

    @api.model
    def create(self, vals):
        re = super(bms_Clinic_report_customer, self).create(vals)
        customers = self.env['res.partner'].search([])
        d1 = datetime.strptime(str(vals['date_start']), '%Y-%m-%d')
        d2 = datetime.strptime(str(vals['date_end']), '%Y-%m-%d')
        count_day = abs((d2 - d1).days)
        for customer in customers:
            HSKB = self.env['bms.clinic.primaryinformation'].search([('name','=',customer.id)])
            list_time = []
            for primarytion in HSKB:
                d3 = datetime.strptime(str(primarytion.dateStart), '%Y-%m-%d')
                count_date = abs((d2 - d3).days)
                list_time.append(count_date)
            list_check = []
            for check in list_time:
                if check <= count_day:
                    list_check.append(check)
            if list_check == list_time and customer.check_customer == True:
                customer.respartner_old_or_new = 'new'
            else:
                customer.respartner_old_or_new = 'old'

        return re