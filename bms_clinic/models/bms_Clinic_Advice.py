# -*- coding: utf-8-*-

from odoo import fields, models,api


# khanh.pg 08.07.2018 lời dặn
class BmsClinicAdvice(models.Model):
    _name = "bms.clinic.advice"
    _description = 'lời dặn'
    _order = "id desc"

    messageCode = fields.Char(string='Mã', readonly=True, compute='_compute_messageCode')
    name = fields.Char(string='Tên lời dặn', required=True)
    classify = fields.Selection([('loidan', 'Lời dặn'), ('cachdung', 'Cách dùng')], string='Phân loại', default='loidan')
    content = fields.Text(string='Nội dung')
    describe = fields.Char(string="Mô tả")
    active = fields.Boolean('Trạng thái', default=True)
    company_id = fields.Many2one('res.company', string="Công ty", default=lambda self: self.env.user.company_id)

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            if record.content != False:
                name = record.content
            else:
                name = record.name
            res.append((record.id, name))
        return res

    @api.multi
    def _compute_messageCode(self):
        for item in self:
            item.messageCode = 'LD' + str(item.id)

