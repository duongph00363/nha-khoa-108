# -*- coding: utf-8 -*-

from odoo import fields, models, api
from odoo.exceptions import Warning
import re


class bms_thuoc(models.Model):
	_inherit = "calendar.event"
	_order = "id desc"

	connect_to = fields.Many2one("bms.clinic.primaryinformation")
	employee_id = fields.Many2one('hr.employee', string='Nha sĩ', default=lambda self: self.sudo().env.uid)
	company_id = fields.Many2one('res.company', string='Thuộc phòng khám')
	customer_id = fields.Many2one('res.partner', string='Khách hàng')
	advice = fields.Text(string='Lời dặn')
	phone = fields.Char(string="Số điện thoại")


