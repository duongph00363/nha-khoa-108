# -*- coding: utf-8 -*-

from odoo import fields, models, api, exceptions
from datetime import datetime
from odoo.exceptions import Warning, ValidationError

import logging

_logger = logging.getLogger(__name__)


# Hồ sơ và khám bệnh
class BmsClinicPrimaryInformation(models.Model):
    _name = 'bms.clinic.primaryinformation'
    _inherit = ["mail.thread"]
    _description = 'Hồ sơ và khám bệnh'
    _order = "id desc"

    name = fields.Many2one(comodel_name='res.partner', string='Họ và tên')
    age = fields.Integer(string='Tuổi', related="name.age")
    address = fields.Char(string='Địa chỉ', related="name.street")
    reasonTo = fields.Char(string='Lý do đến khám')
    # field này hiện tại để invisible khong dung den
    doctorSpecified = fields.Many2one('hr.employee', string='Bác sĩ chỉ định', default=lambda self: self.sudo().env.uid)
    doctor_name = fields.Char(string='Tên bác sĩ',
                              related='doctorSpecified.name', store=True)
    profileCode = fields.Char(string='Mã hồ sơ', readonly=True, compute='_compute_profileCode')
    phoneNumber = fields.Char(string='Số điện thoại', related="name.phone")
    sex = fields.Selection(string='Giới tính', related="name.sex")
    dateStart = fields.Date(string='Ngày tạo', default=datetime.today())
    all_total_money = fields.Float(string='Tổng tiền thanh toán', compute='depends_sale_order')
    totalService = fields.Float(string='Tiền được chiết khấu', compute='depends_sale_order')
    money_card = fields.Float(string="Chiết khấu bằng điểm",compute='depends_sale_order')
    totalDiscountedAmount = fields.Float(string='Tổng tiền giảm trừ')
    totalAmountPaid = fields.Float(string='Đã thanh toán', compute='depends_payment')
    totalAmountBePaid = fields.Float(string='Tổng tiền cần thanh toán', compute='depends_bepaid')
    # status create ho so . moi : save đang điều trị, cần thanh toán =0 haonf tất , tất cả các dịnh vụ hoàn thành : điều trị hoàn tất
    status = fields.Selection([('new', 'Mới'), ('notdone', 'Đang điều trị'), ('done', 'Hoàn thành thanh toán'),
                               ('done2', 'Hoàn tất điều trị'), ('close', 'Đóng'), ('cancel', 'Hủy')],
                              string='Trạng thái', default='new', track_visibility='onchange', readonly=True,
                              copy=False, index=True, )
    state = fields.Selection([('new', 'Mới'), ('notdone', 'Đang điều trị'), ('done', 'Hoàn thành thanh toán'),
                              ('done2', 'Hoàn tất điều trị'), ('close', 'Đóng'), ('cancel', 'Hủy')],
                             string='Trạng thái', default='new', compute="depends_bepaid")
    dateEnd = fields.Datetime(string='Ngày kết thúc')
    prescription_ids = fields.One2many('bms.clinic.monocuminformation', 'monocum_id', string='Kê đơn')
    clsresult_ids = fields.One2many('bms.clinic.clsresult', 'primaryInformation_id', string='Kết quả khám lâm sàng')
    _sale_order_ids = fields.One2many(comodel_name='sale.order', inverse_name='PrimaryInformation_id', string="Danh sách dịch vụ")
    # chỉ định dịch vụ
    specifiedSevices_ids = fields.One2many(comodel_name='sale.order.line', inverse_name='specifiedSevices_id',
                                           string="Chỉ định dịch vụ", store=True)

    service = fields.Char(related='_sale_order_ids.order_line.product_id.name')
    # Liệu trình điều trị
    bms_remedy_ids = fields.One2many('bms.clinic.remedy', 'primaryinformation_remedy_id', string='Liệu trình điều trị',
                                     store=True)
    # bms_remedy_ids_depend = fields.Char("Test", compute='_move_data')
    # Hồ sơ bệnh nhân
    profile_id = fields.Many2one('res.partner', string='profile_id')
    check_paid = fields.Boolean(string='Check paid', default=False)
    company_id = fields.Many2one('res.company', string="Công ty", default=lambda self: self.env.user.company_id)
    notes = fields.Text(string="Ghi chú:")

    # tong dich vu
    all_service = fields.Many2many('product.product', string='dich vu', compute='_comupute_servic')
    # Lịch hẹn
    meeting_ids = fields.One2many('calendar.event', "connect_to", string='Meetings')
    meeting_count = fields.Integer('# Lịch hẹn', compute='_compute_meeting_count')
    payment = fields.One2many("account.payment","profileCoce_primaryInformation")

    #tong tien da thanh toan
    tongTienDaThanhToan = fields.Float('Tổng tiền đã thanh toán', compute='compute_tongTienDaThanhToan')
    indebtedness = fields.Float(string="Nợ",compute='compute_indebtedness',readonly=True)
    befor_discount = fields.Float('Tổng tiền trước chiết khấu', default=0, compute='depends_sale_order')
    company_current_id = fields.Many2one('res.company', string="Công ty hieenj taij", compute='compute_company_current_id')
    company_current = fields.Integer(string="Công ty hiện tại", compute='compute_company_current_id')
    cancellation_of_service = fields.Float(string='Giảm trừ do hủy dịch vụ', compute='depends_sale_order')
    sum_paid = fields.Float("Tổng tiền trả lại khách",compute="depends_payment")

    @api.multi
    @api.depends('company_current_id', 'company_current')
    def compute_company_current_id(self):
        for item in self:
            item.company_current_id = self.env.user.company_id
            if item.company_id.id == item.company_current_id.id:
                item.company_current = 1
            else:
                item.company_current = 0

    #kiểm tra xem phieeys khám vừa tạo có dịch vụ nào hay không
    @api.multi
    @api.onchange('_sale_order_ids')
    def comput_sale_order_line(self):
        for item in self._sale_order_ids:
            if len(item.order_line) == 0:
                raise ValidationError('Phiếu khám bạn vừa tạo hiện chua có dịch vụ nào !')

    @api.multi
    def compute_indebtedness(self):
        for item in self:
            item.indebtedness = item.all_total_money - item.tongTienDaThanhToan
            indebtedness = item.all_total_money - item.tongTienDaThanhToan
        return indebtedness
        # nếu số tiền tạm ứng lớn hơn hoặc bằng số tiền thanh toán thì trạng thái chuyển sang hoàn thành thanh toán

    # tinh tong tien da thanh toan
    @api.multi
    def compute_tongTienDaThanhToan(self):
        for item in self:
            account_payment = self.env['account.payment'].sudo().search([('profileCoce_primaryInformation', '=', item.id)])
            for rec in account_payment:
                item.tongTienDaThanhToan += rec.amount

    @api.multi
    def action_search_remedy(self):
        action = self.env.ref('bms_clinic.bms_clinnc_remedy_act_view').read()[0]
        action['domain'] = str([('primaryinformation_remedy_id', '=',self.id)])
        return action

    @api.multi
    def action_search_tamung(self):
        action = self.env.ref('bms_clinic.bms_clinic_account_payment_action').read()[0]
        action['domain'] = str([('profileCoce_primaryInformation', '=',self.id)])
        return action

    @api.multi
    def action_search_thanhtoan(self):
        action = self.env.ref('bms_clinic.bms_clinic_action_view').read()[0]
        action['domain'] = str([('origin', '=',self.profileCode)])
        return action

    @api.multi
    def action_search_calendar(self):
        action = self.env.ref('bms_clinic.bms_clinic_action_view').read()[0]
        action['domain'] = str([('profileCoce_primaryInformation', '=',self.id)])
        return action

    @api.multi
    def _compute_meeting_count(self):
        for partner in self:
            partner.meeting_count = len(partner.meeting_ids)

    @api.multi
    def schedule_meeting(self):
        partner_ids = self.ids
        partner_ids.append(self.env.user.partner_id.id)
        action = self.env.ref('calendar.action_calendar_event').read()[0]
        action['domain'] = str([('connect_to', '=', self.id)])
        action['context'] = {
            # 'search_default_connect_to': self.id,
            'default_partner_ids': partner_ids,
        }
        return action

    # thay dổi trạng thái của status
    @api.multi
    @api.depends('totalAmountBePaid')
    def _compute_status(self):
        for item in self:
            if item.totalAmountBePaid == 0:
                item.status = 'done'
            else:
                item.status = 'notdone'

    @api.multi
    @api.depends('_sale_order_ids','totalAmountPaid','cancellation_of_service','specifiedSevices_ids')
    def depends_sale_order(self):
        for order in self:
            befor_discount = 0
            for line in order._sale_order_ids:
                befor_discount = befor_discount + line.amount_total
            list1 = []
            list2 = []
            for canse in order.specifiedSevices_ids:
                if canse.active_cancel == True:
                    list1.append(canse.money_collected)
                if canse.active_cancel == False:
                    list2.append(canse.price_subtotal)
            if len(list1) != 0:
                cancellation_of_service = befor_discount - sum(list1) - sum(list2)
                all_total_money = befor_discount - cancellation_of_service
            if len(list1) == 0:
                cancellation_of_service = 0
                all_total_money = befor_discount
            order.update({
                'all_total_money': all_total_money,
                'cancellation_of_service': cancellation_of_service,
                'befor_discount': befor_discount,
            })

    @api.multi
    @api.depends('totalAmountPaid', 'sum_paid')
    def depends_payment(self):
        for order in self:
            payments = self.env['account.payment'].sudo().search(
                [('profileCoce_primaryInformation', '=', order.id), ('bms_payment_reasons', '=', order.profileCode),
                 ('payment_type', '=', 'inbound')])
            totalAmountPaid = 0
            for payment in payments:
                for money in payment:
                    totalAmountPaid = totalAmountPaid + money.amount
            sum_paids = self.env['account.payment'].sudo().search(
                [('profileCoce_primaryInformation', '=', order.id), ('bms_payment_reasons', '=', order.profileCode),
                 ('payment_type', '=', 'outbound')])
            sum_money = 0
            for sum_paid in sum_paids:
                for sums in sum_paid:
                    sum_money = sum_money + sums.amount
            totalAmountBePaid = order.all_total_money - totalAmountPaid + sum_money
            order.update({
                'totalAmountPaid': totalAmountPaid,
                'sum_paid': sum_money,
                'totalAmountBePaid': totalAmountBePaid,
            })

    @api.multi
    @api.depends('totalAmountPaid', 'sum_paid','totalAmountBePaid','all_total_money','cancellation_of_service','_sale_order_ids')
    def depends_bepaid(self):
        for order in self:
            totalAmountBePaid = order.all_total_money - order.totalAmountPaid + order.sum_paid
            if len(order._sale_order_ids) == 0:
                state = 'new'

            if len(order._sale_order_ids) != 0:
                state = 'notdone'

            if totalAmountBePaid == 0 and len(order._sale_order_ids) != 0:
                state = 'done'
            invoice = self.env['account.invoice'].search([('origin', '=', order.profileCode)])
            print(len(invoice))
            if len(invoice) == 1:
                state = 'close'
            order.update({
                'totalAmountBePaid': totalAmountBePaid,
                'state': state,
            })

    @api.multi
    @api.depends('specifiedSevices_ids', 'cancellation_of_service')
    def compute_cancellation_of_service(self):
        total = 0
        for items in self.specifiedSevices_ids:
            if items.active_cancel == True:
                total += (items.price_subtotal - items.money_collected)
        self.cancellation_of_service = total


    @api.multi
    def open_form_profile(self):
        val = self.env['sale.order.line'].sudo().search_read([('order_id', '=', self.customer.id)])
        sales_order_line_id = []
        for val_id in val:
            sales_order_line_id.append(val_id['id'])
        self.sales_order_line_ids = sales_order_line_id
        compose_form = self.env.ref('view_order_form', False)
        ctx = dict(
            default_model='bms.clinic.primaryinformation',
            default_partner_id=self.name.id
        )
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'sale.order',
            'views': [(compose_form, 'form')],
            'view_id': compose_form,
            'target': 'new',
            'context': ctx,
        }


    @api.multi
    def open_form_customerinvoices_invoice(self):
        view_id = False
        ctx = dict(
            default_model='bms.clinic.primaryinformation',
            # search_default_profileCoce_primaryInformation=self.profileCode,
        )
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'account.invoice',
            'view_id': view_id,
            'context': ctx,
            'flags': {'action_buttons': True},
        }


    @api.multi
    def _compute_profileCode(self):
        for item in self:
            item.profileCode = 'HSKH' + str(item.id)

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            name = record.profileCode
            res.append((record.id, name))
        return res

    # so sanh dich vu
    @api.one
    @api.depends('specifiedSevices_ids')
    def _comupute_servic(self):
        all_sv = []
        for id_service in self.specifiedSevices_ids:
            all_sv.append(id_service.product_id.id)
        self.all_service = all_sv

    @api.multi
    def action_lock(self):
        self.env.context = {}
        previous_payment_ids = self.env['account.payment'].search(
            [('company_id', '=', self.company_id.id), ('bms_payment_reasons', '=', self.profileCode),('payment_method_id','=','inbound')])
        print(previous_payment_ids)
        order_ids = []
        account_511 = self.env['account.account'].search(
            [('name', 'like', 'Doanh thu bán hàng hóa'), ('company_id', '=', self.company_id.id)])
        for item in self._sale_order_ids:
            for i in item.order_line:
                order_ids.append([0, 0, {'product_id': i.product_id.id,
                                         'discount': i.discount,
                                         'quantity': i.product_uom_qty,
                                         'uom_id': i.product_uom.id,
                                         'price_unit': i.price_unit,
                                         'sequence': i.sequence,
                                         'name': i.name,
                                         'account_id': account_511.id,
                                         'company_id': self.company_id.id,
                                         }])
        inv = self.env['account.invoice'].create({'partner_id': self.name.id,
                                                  'invoice_line_ids': order_ids,
                                                  'payment_method_id': 1,
                                                  'origin': self.profileCode,
                                                  'company_id': self.company_id.id,
                                                  'amount_point': self.money_card,
                                                  })
        print(inv)
        inv.action_invoice_open()

        for previous_payment_id in previous_payment_ids:
            previous_payment_id.invoice_ids = inv
            previous_payment_id.has_invoices = True
            previous_payment_id.payment_method_id = 1
            previous_payment_id.post()



    @api.multi
    def action_cancel(self):
        for state in self._sale_order_ids:
            if state.state != 'darft':
                raise Warning('Dịch vụ đã được xác nhận. không thể hủy')
        self.write({'state': 'cancel'})



    @api.model
    def create(self, vals):
        rc = super(BmsClinicPrimaryInformation, self).create(vals)
        if rc._sale_order_ids:
            for id_so in rc._sale_order_ids:
                sale_order = self.env['sale.order'].browse(id_so.id)
                sale_order.action_confirm()
        return rc

    @api.multi
    def write(self, vals):
        rc = super(BmsClinicPrimaryInformation, self).write(vals)
        for id_so in self._sale_order_ids:
            if id_so.state != 'sale':
                sale_order = self.env['sale.order'].browse(id_so.id)
                sale_order.action_confirm()
        return rc


