# -*- coding: utf-8 -*-

from odoo import fields, models, api
from datetime import datetime
from odoo.exceptions import Warning
from odoo.exceptions import UserError, ValidationError



class BmsClinicSaleOrder(models.Model):
	_inherit = 'sale.order'

	PrimaryInformation_id = fields.Many2one('bms.clinic.primaryinformation', string='Hồ sơ khám bệnh')
	doctor = fields.Many2one('hr.employee', string='Bác sĩ')
	doctor_sp = fields.Many2one('hr.employee', string='Bác sĩ hỗ trợ')
	Content_of_results = fields.Text(string='Nọi dung kết quả thực hiện')
	perform_id = fields.One2many(comodel_name="clinic.perform.service", inverse_name="saleorder_id",
								 string="Thực hiện dịch vụ")
