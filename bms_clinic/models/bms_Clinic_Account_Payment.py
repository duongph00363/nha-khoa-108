# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import Warning, ValidationError



class bms_Clinic_account_payment(models.Model):
    _inherit = 'account.payment'
    _order = "id desc"

    bms_payment_reasons = fields.Char(string='Lý do thanh toán')
    money_read = fields.Char(string='Số tiền', compute='_compute_money_read')
    profileCoce_primaryInformation = fields.Many2one('bms.clinic.primaryinformation', string='Hồ sơ')
    name = fields.Char(readonly=True, copy=False, default="Phiếu tạm ứng")  # The name is attributed upon post()
    check_readonly = fields.Boolean('check readonly')
    phone = fields.Char(string="Số điện thoại",related='partner_id.phone')

    @api.multi
    def xuly(self, number):
        lst = []
        while number // 10 != 0:
            du = number % 10
            number = number // 10
            lst.append(du)
        lst.append(number)
        lst.reverse()
        return lst

    # @api.onchange('amount')
    # def compute_amount(self):
    #     if self.amount > self.bms_info:
    #         raise Warning('Số tiền thanh toán đang lớn hơn số tiền cần thanh toán !')

    @api.multi
    def tinhtoan(self):
        str_num = {
            0: 'Không',
            1: 'Một',
            2: 'Hai',
            3: "Ba",
            4: 'Bốn',
            5: 'Năm',
            6: 'Sáu',
            7: 'Bảy',
            8: 'Tám',
            9: 'Chín'
        }

        hang = {
            12: ' nghìn ',
            11: ' trăm ',
            10: ' mươi ',
            9: ' tỷ ',
            8: ' trăm ',
            7: ' mươi ',
            6: ' triệu ',
            5: ' trăm ',
            4: ' mươi ',
            3: ' nghìn ',
            2: ' trăm ',
            1: ' mươi ',
        }

        number = int(self.amount)
        number_in_text = ''
        money_in_text = ''
        if number > 1000 and number < 999999 and number % 1000 == 0:
            number /= 1000
            lst_num = self.xuly(number)
            # print(lst_num)
            # write(xuly(2145))
            pos = len(lst_num)
            string = ''

            for i in lst_num:
                pos -= 1
                if pos == 0 and i == 0:
                    string += ''
                elif pos == 0:
                    string += str_num[i]
                # elif pos == 3 and i == 0 and lst_num[len(lst_num) - 3] == 0:
                #     string += ''
                # elif pos == 2 and i == 0 and lst_num[len(lst_num) - 2] == 0:
                #     string += ''
                elif pos == 1 and i == 0 and lst_num[len(lst_num) - 1] == 0:
                    string += ''
                elif pos == 1 and i == 0:
                    string += 'lẻ '
                elif pos == 4 and i == 0:
                    string += 'lẻ '
                elif pos == 7 and i == 0:
                    string += 'lẻ '
                elif pos == 1 and i == 1:
                    string += 'Mười '
                elif pos == 4 and i == 1:
                    string += 'Mười '
                elif pos == 7 and i == 1:
                    string += 'Mười '
                else:
                    string += str_num[i] + hang[pos]
            number_in_text = string + " nghìn"
        elif number > 1000000 and number < 999999999 and number % 1000000 == 0:
            number /= 1000000
            lst_num = self.xuly(number)
            # print(lst_num)
            # write(xuly(2145))
            pos = len(lst_num)
            string = ''

            for i in lst_num:
                pos -= 1
                if pos == 0 and i == 0:
                    string += ''
                elif pos == 0:
                    string += str_num[i]
                elif pos == 1 and i == 0 and lst_num[len(lst_num) - 1] == 0:
                    string += ''
                elif pos == 1 and i == 0:
                    string += 'lẻ '
                elif pos == 4 and i == 0:
                    string += 'lẻ '
                elif pos == 7 and i == 0:
                    string += 'lẻ '
                elif pos == 1 and i == 1:
                    string += 'Mười '
                elif pos == 4 and i == 1:
                    string += 'Mười '
                elif pos == 7 and i == 1:
                    string += 'Mười '
                else:
                    string += str_num[i] + hang[pos]
            number_in_text = string + " triệu"
        elif number > 1000000000 and number < 999999999999 and number % 1000000000 == 0:
            number /= 1000000000
            lst_num = self.xuly(number)
            # print(lst_num)
            # write(xuly(2145))
            pos = len(lst_num)
            string = ''

            for i in lst_num:
                pos -= 1
                if pos == 0 and i == 0:
                    string += ''
                elif pos == 0:
                    string += str_num[i]
                # elif pos == 3 and i == 0 and lst_num[len(lst_num) - 3] == 0:
                #     string += ''
                # elif pos == 2 and i == 0 and lst_num[len(lst_num) - 2] == 0:
                #     string += ''
                elif pos == 1 and i == 0 and lst_num[len(lst_num) - 1] == 0:
                    string += ''
                elif pos == 1 and i == 0:
                    string += 'lẻ '
                elif pos == 4 and i == 0:
                    string += 'lẻ '
                elif pos == 7 and i == 0:
                    string += 'lẻ '
                elif pos == 1 and i == 1:
                    string += 'Mười '
                elif pos == 4 and i == 1:
                    string += 'Mười '
                elif pos == 7 and i == 1:
                    string += 'Mười '
                else:
                    string += str_num[i] + hang[pos]
            number_in_text = string + " tỷ"
        else:
            lst_num = self.xuly(number)
            # write(xuly(2145))
            pos = len(lst_num)
            string = ''
            for i in lst_num:
                pos -= 1
                if pos == 0 and i == 0:
                    string += ''
                elif pos == 0:
                    string += str_num[i]
                elif pos == 1 and i == 0:
                    string += 'lẻ '
                elif pos == 4 and i == 0:
                    string += 'lẻ '
                elif pos == 7 and i == 0:
                    string += 'lẻ '
                elif pos == 1 and i == 1:
                    string += 'Mười '
                elif pos == 4 and i == 1:
                    string += 'Mười '
                elif pos == 7 and i == 1:
                    string += 'Mười '
                else:
                    string += str_num[i] + hang[pos]
            number_in_text = string
        return number_in_text

    @api.multi
    @api.depends('amount')
    def _compute_money_read(self):
        """ Tính tiền ra  chữ """
        number_text = self.tinhtoan() + ' đồng'
        self.money_read = number_text.lower()
