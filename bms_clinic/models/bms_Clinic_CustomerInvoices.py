# -*- coding: utf-8 -*-

from odoo import models, fields, api


class BmsClinicCustomerInvoice(models.Model):
    _name = 'bms.clinic.custominvoices'
    _order = "id desc"

    profile_code = fields.Char(string='Mã hồ sơ')
    customer = fields.Many2one('res.partner', string='Tên khách hàng')
    address = fields.Char(string='Địa chỉ')
    sales_order_line_ids = fields.One2many('sale.order.line', 'name', )
    account_payment_ids = fields.One2many('account.payment', 'name', compute='_compute_payment_ids')
    bms_untaxed_amount = fields.Float(string='Tổng tiền', readonly="1")
    bms_amount_tax = fields.Float(string='Tax', readonly="1")
    bms_amount_total = fields.Float(string='Total', readonly="1")
    bms_residual = fields.Float(string='Amount Due', compute='_compute_bms_residual')
    company_id = fields.Many2one('res.company', string="Công ty", default=lambda self: self.env.user.company_id)


    @api.model
    @api.depends('bms_residual')
    def _compute_bms_residual(self):
        residual = 123
        self.bms_residual = residual

    @api.multi
    @api.depends('account_payment_ids', 'customer')
    def _compute_payment_ids(self):
        val = self.env['account.invoice'].search_read([('partner_id', '=', self.customer.id)])
        payment_id = []
        for val_id in val:
            payment_id.append(val_id['id'])
        self.account_payment_ids = payment_id
