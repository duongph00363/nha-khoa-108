# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import date

#Nhân viên
class BmsClinicEmployees(models.Model):
    _inherit = 'hr.employee'
    _description = 'Nhân viên'
    _order = "id desc"

    employees_id = fields.Char(string='ID nhân viên', compute='_compute_employees_id')
    cash_base = fields.Float(string='Lương cơ bản')
    date_start = fields.Date(string='Ngày bắt đầu', default=date.today())
    company_id = fields.Many2one('res.company', string='Phòng khám')

    @api.multi
    def _compute_employees_id(self):
        for item in self:
            item.employees_id = 'NV' + str(item.id)
