# -*- coding: utf-8 -*-


from odoo import fields, models, api


class BmsClinic_report_payment(models.Model):
    _inherit = 'account.payment'

    address = fields.Char(related="partner_id.street")
    phone = fields.Char(related="partner_id.phone")
    birthday = fields.Date(related="partner_id.birthday")
    info = fields.Char("Loại phiếu")
    acc_invoices = fields.Many2one('account.invoice')
    bms_info = fields.Float('bms.clinic.primaryinformation')


    @api.multi
    @api.onchange('payment_type','partner_type')
    def on_change_state(self):
        for record in self:
            if record.payment_type == 'inbound' and record.partner_type == 'customer':
                self.info = "Phiếu thu khách hàng"

            elif record.payment_type == 'outbound' and record.partner_type == 'customer':
                self.info = "Phiếu chi khách hàng"

            elif record.payment_type == 'outbound' and record.partner_type == 'supplier':
                self.info = "Phiếu chi nhà cung cấp"

            elif record.payment_type == 'inbound' and record.partner_type == 'supplier':
                self.info = "Phiếu thu nhà cung cấp"
