# -*- coding: utf-8 -*-

from odoo import fields, models,api

class BmsClinicUnitCategory(models.Model):
    _inherit = 'product.uom'

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            name = record.name
            res.append((record.id, name))
        return res