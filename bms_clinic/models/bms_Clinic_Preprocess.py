# -*- coding: utf-8 -*-

from odoo import fields, models, api

#Tiền sử bệnh nhân
class BmsClinicPreprocess(models.Model):
    _name = 'bms.clinic.preprocess'
    _description = 'Tiền sử bệnh nhân'
    _order = "id desc"

    preprocess_code = fields.Char(string='Mã tiền sử', compute='_compute_preprocess_code')
    name = fields.Char(string='Tên tiền sử', required=True)
    description = fields.Text(string='Mô tả')
    partner_id = fields.Many2one('res.partner', string='Tên bệnh nhân')
    active = fields.Boolean('Trạng thái', default=True)
    company_id = fields.Many2one('res.company', string="Công ty", default=lambda self: self.env.user.company_id)

    @api.multi
    def _compute_preprocess_code(self):
        for item in self:
            item.preprocess_code = 'TS'+ str(item.id)
