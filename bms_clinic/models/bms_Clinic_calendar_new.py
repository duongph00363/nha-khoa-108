# -*- coding: utf-8 -*-

from odoo import api, exceptions, fields, models


class bms_primaryinformation(models.Model):
    _inherit = "bms.clinic.primaryinformation"

    # Button tao lich hen
    @api.multi
    def open_form_calendar(self):
        return_view = self.env.ref('calendar.event.form', False)
        ctx = dict(
            default_model='bms.clinic.primaryinformation',
            default_customer_id=self.name.id,
            default_company_id = self.company_id.id,
            default_location = self.company_id.street,
            default_connect_to= self.id,
            default_phone =self.phoneNumber,
        )
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'calendar.event',
            'views': [(return_view, 'form')],
            'view_id': return_view,
            'target': 'current',
            'context': ctx,
        }
