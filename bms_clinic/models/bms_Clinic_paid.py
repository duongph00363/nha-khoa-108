# -*- coding: utf-8 -*-

from odoo import fields, models, api
from datetime import datetime
from odoo.exceptions import Warning
from odoo.exceptions import UserError, ValidationError


class BmsClinicPaid(models.Model):
    _inherit = 'bms.clinic.primaryinformation'


    @api.multi
    def open_paid(self):
        tamung = self.env['account.payment'].search([('bms_payment_reasons','=',self.profileCode)])
        sum = 0
        for money in tamung:
            sum = sum + money.amount
        journal_cash_id = self.env['account.journal'].sudo().search([('type', '=', 'cash'),
                                                                     ('company_id', '=', self.company_id.id)])
        compose_form = self.env.ref('account.payment.form', False)

        ctx = dict(
            default_model = 'bms.clinic.primaryinformation',
            default_payment_type='outbound',
            default_partner_type='customer',
            default_partner_id = self.name.id,
            default_journal_id = journal_cash_id.id,
            default_company_id = self.company_id.id,
            default_profileCoce_primaryInformation = self.id,
            default_bms_payment_reasons = self.profileCode,
            default_check_readonly = True,
            default_amount = -self.totalAmountBePaid,
        )

        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'account.payment',
            'views': [(compose_form, 'form')],
            'view_id': compose_form,
            'target': 'current',
            'context': ctx,
        }

