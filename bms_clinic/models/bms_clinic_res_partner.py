# -*- coding: utf-8 -*-

from odoo import fields, models, api
from datetime import datetime, timedelta
from odoo.exceptions import UserError, ValidationError, Warning
import re,math

# khách hàng
class bms_res_partner(models.Model):
    _inherit = "res.partner"
    _description = 'khách hàng'
    _order = "id desc"

    bar_code = fields.Char(string="Mã hồ sơ", readonly=True, compute='_compute_bar_code')
    age = fields.Integer(string="Tuổi")
    date_start = fields.Date(string="Ngày tạo hồ sơ", default=datetime.today())
    reason = fields.Char(string="Lý do đến khám",)
    status = fields.Selection(selection=[("1", "Tạo mới"), ("2", "Đang thực hiện"), ("3", "Hoàn thành")],
                              string="trạng thái", default="1")
    address = fields.Many2one('res.company', string='Địa chỉ', default=lambda self: self.env.user.company_id)
    resource_custom = fields.Many2one('bms.clinic.resourcerefcustom', string='Nguồn khách')
    birthday = fields.Date(string='Ngày sinh', default=datetime.today())
    sex = fields.Selection([('male', 'Nam'), ('female', 'Nữ')], string='Giới tính', default='male')

    # date_import = fields.Date(string='Ngày nhập', default=date.today())
    #Tiền sử bệnh nhân
    preprocess = fields.One2many('bms.clinic.preprocess.detail','partner_id', string='Tiền sử bệnh nhân')
    # hồ sơ khám bệnh
    primary_ids = fields.One2many('bms.clinic.primaryinformation', 'name', string='Danh sách hồ sơ khám bệnh')
    profile_count = fields.Integer("# Hồ sơ khám bệnh", compute='_compute_action_profile_count')
    # button hồ sơ khám bệnh
    profile_ids = fields.Many2many('bms.clinic.primaryinformation', 'calendar_event_res_partner_rel', 'res_partner_id',
                                   'calendar_event_id', string='Meetings', copy=False)
    primaryInformation_ids = fields.One2many('bms.clinic.primaryinformation', 'name', string='Hồ sơ khám bệnh')
    account_payment_ids = fields.One2many('account.payment','partner_id',string='Danh sách tạm ứng')
    service_ids = fields.One2many('sale.order.line','order_partner_id', string="Danh sách dịch vụ")
    bms_clinic_remedy_ids = fields.One2many("bms.clinic.remedy", "partner_id", string="liệu trình điều trị")

    respartner_old_or_new = fields.Selection([('old', "Cũ"), ('new', "Mới")],string='Phân loại', default='new')
    date_start_create = fields.Datetime(string='Ngày tạo hồ sơ đầu', compute='compute_date_start_create_date_start_late')
    date_start_late = fields.Datetime(string='Ngày tạo hồ sơ gần nhất', compute='compute_date_start_create_date_start_late')
    check_customer = fields.Boolean(string='Khách hàng mới ?',default= True)


    @api.depends('auto_dating_send_time')
    def _compute_remind_dating_send_time(self):
        for record in self:
            time = record.auto_dating_send_time
            string = str(math.floor(time)) + ':' + str(round((time - math.floor(time)) * 60)) + ':00'
            send_time = (datetime.utcnow() + timedelta(days=0)).strftime('%Y-%m-%d ' + string)
            cron_jobs = self.env['ir.cron'].sudo().search([('name', '=', 'Remind dating')])
            for cron in cron_jobs:
                cron.write({'nextcall': send_time})

    @api.multi
    def compute_date_start_create_date_start_late(self):
        for item in self:
            if item.primaryInformation_ids:
                item.date_start_late = item.primaryInformation_ids[0].dateStart
                item.date_start_create = item.primaryInformation_ids[-1].dateStart

    @api.multi
    def open_primaryinformation(self):
        compose_form = self.env.ref('bms_PrimaryInformation_form_view', False)
        ctx = dict(
            default_model='res.partner',
            default_name=self.id,
        )
        return {
            # 'name': _('Compose Email'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'bms.clinic.primaryinformation',
            'views': [(compose_form, 'form')],
            'view_id': compose_form,
            'target': 'current',
            'context': ctx,
        }


    @api.onchange('name')
    def check_name_character(self):
        for item in self:
            if item.name:
                item.name = str(item.name).upper()


    @api.multi
    @api.constrains('phone', 'birthday')
    def _check_validate_phone_number(self):
        for rec in self:
            if rec.phone and (len(rec.phone) < 10 or len(rec.phone) > 11):
                raise ValidationError("Số điện thoại  gồm 10 hoặc 11 số !")
            if rec.age < 0:
                raise ValidationError('Độ tuổi chưa hợp lệ, vui lòng kiểm tra lại ngày sinh !')
        return True

    @api.onchange('email')
    def check_validate_mail(self):
        if self.email:
            match = re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', self.email)
            if match == None:
                raise ValidationError('Định dạng email bạn nhập chưa đúng')

    @api.model
    @api.depends('primary_ids', 'profile_count')
    def _compute_action_profile_count(self):
        for partner in self:
            partner.profile_count = len(partner.primary_ids)



    @api.multi
    @api.onchange('birthday')
    def age_calc(self):
        for record in self:
            record.profile_count = len(record.primary_ids)
            if record.birthday is not False:
                record.age = (datetime.today().date() - datetime.strptime(record.birthday,
                                                                          '%Y-%m-%d').date()) // timedelta(days=365)



    @api.model
    @api.depends('primary_ids', 'profile_count')
    def _compute_action_profile_count(self):
        for partner in self:
            partner.profile_count = len(partner.primary_ids)

    @api.multi
    def action_profile_count(self):
        # profile_ids = self.ids
        # print(profile_ids)
        profile_ids = self.env.user.partner_id
        print(profile_ids)
        action = self.env.ref('bms_clinic.bms_clinic_primaryinfirmation_action_view').read()[0]
        action['context'] = {
            # 'search_default_name': 'active_id',
            'default_partner_ids': profile_ids,
        }
        return action

    @api.multi
    def _compute_bar_code(self):
        for item in self:
            item.bar_code = 'KH' + str(item.id)


    @api.model
    def create(self, vals):
        re = super(bms_res_partner, self).create(vals)
        re['name'] = re['name'].upper()
        return re

    @api.multi
    def write(self, vals):
        if self.profile_count > 1 and (self.date_start_create[5:7] != self.date_start_late[5:7]):
            vals['respartner_old_or_new'] = 'old'
        return super(bms_res_partner,self).write(vals)


class bmsPreprocessDetail(models.Model):
    _name = 'bms.clinic.preprocess.detail'
    _description = 'Chi tiết tiền xửu bệnh nhân'

    preprosess_id = fields.Many2one('bms.clinic.preprocess', string='Tên tiền sử')
    description_preprocess = fields.Char(string='Mô tả')
    partner_id = fields.Many2one('res.partner')
    company_id = fields.Many2one('res.company', string="Công ty", default=lambda self: self.env.user.company_id)

    @api.multi
    @api.onchange('preprosess_id')
    def onchange_preprosess_id(self):
        if self.description_preprocess == False:
            self.description_preprocess = self.preprosess_id.description
