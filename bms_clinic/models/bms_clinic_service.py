# -*- coding: utf-8 -*-

from odoo import fields, models, api
from odoo.exceptions import Warning



class bms_service_line(models.Model):
    _inherit = "sale.order.line"


    discount_amount = fields.Float("Giá tiền giảm", compute="_compute_discount_amount")
    standard_price = fields.Float("Chi phí xưởng", related="product_id.standard_price")
    #tạo mới lần đầu, lần cuối cùng là hoàn thành , nhưng lần ở giữa là đang thực hiên
    bms_status = fields.Selection([(1, 'Tạo mới'), (2, 'Đang thực hiện'), (3, 'Hoàn thành')], string='Trạng thái',
                                  default='2')
    content = fields.Char("Nội dung thực hiện")
    #dich vụ chỉ định
    # specifiedSevices_id = fields.Many2one('bms.clinic.primaryinformation', string="Thông tin hành chinh")
    specifiedSevices_id = fields.Many2one(related='order_id.PrimaryInformation_id')
    # liệu trình điều trị
    order_line_remedy_id = fields.One2many('bms.clinic.remedy', 'so_line_ids', 'lieu trinh')
    doctor = fields.Many2one("hr.employee", string='Người điều trị', required=True)
    doctor_sp = fields.Many2one("hr.employee", string='Người tư vấn')

    # class BmsClinic_report_specifiedSevices(models.Model):
    connect = fields.Many2one("bms.clinic.primaryinformation")
    name_customer = fields.Char(related="order_partner_id.name")
    date_id = fields.Date(related="order_id.PrimaryInformation_id.dateStart")
    doctor_name = fields.Many2one(related="order_id.PrimaryInformation_id.doctorSpecified")

    # class Bms_clinic_related(models.Model):
    phone_id = fields.Char(string='phone',
                           related='order_partner_id.phone')
    street_id = fields.Char(string='Địa Chỉ',
                            related='order_partner_id.street')
    ward_id = fields.Char(string='Quận')
    district_id = fields.Char(string='Huyện')
    arel = fields.Many2one(comodel_name='bms.clinic.primaryinformation', string='connect.primaryinfomation')
    doctor_id = fields.Char(string='Bác sỹ khám',
                            related='arel.doctorSpecified.name')
    age_id = fields.Integer(string='Tuổi', related="arel.age", store=True)

    #class bmsClinicSaleorderline(models.Model):
    csinvoice_id = fields.Many2one('bms.clinic.custominvoices')

    # class BmsClinicReportLoan(models.Model):
    connect_to_primaryinformation = fields.Many2one(comodel_name='bms.clinic.primaryinformation',
                                                    string='connect.primaryinformation')
    loan = fields.Float(string='Nợ', related='connect_to_primaryinformation.totalAmountBePaid', sotre=True)

    price_befor_discount = fields.Float(string='Giá', related='product_id.lst_price', store=True,readonly=True)
    unit_discount = fields.Float('Khuyến mãi (%)', default=0, compute="_compute_promotion",store=True)

    active_cancel = fields.Boolean(string='Hủy dịch vụ', default=False)
    money_collected = fields.Float(string='Số tiền cần thu từ khách hàng', default='0')
    age = fields.Char(string='Tuổi')
    total_money_collected = fields.Float(string='Tiền giảm trừ', compute='compute_total_money_collected', store=True)

    @api.multi
    @api.depends('active_cancel')
    def compute_total_money_collected(self):
        for item in self:
            if item.active_cancel == True:
                item.total_money_collected += item.money_collected
            else:
                item.total_money_collected += item.price_subtotal



    @api.onchange('money_collected')
    def compute_onchange_cancellation_of_service(self):
        if self.money_collected > self.price_subtotal:
            raise Warning('Số tiền giảm trừ đang lớn hơn giá trị của dịch vụ !')


    @api.multi
    def cancel_service(self):
        if self.active_cancel == False:
            self.active_cancel = True

    @api.one
    @api.depends("product_id", "price_befor_discount", "price_unit")
    def _compute_promotion(self):
        if self.price_unit != 0 and self.price_befor_discount != 0:
            item = (self.price_befor_discount - self.price_unit)/self.price_befor_discount * 100
            self.update({'unit_discount': item})

    @api.multi
    @api.depends("price_unit", "product_uom_qty", "discount")
    def _compute_discount_amount(self):
        for amount in self:
            if amount.price_befor_discount and amount.unit_discount:
                amount.discount_amount = (amount.price_befor_discount * amount.product_uom_qty * (amount.unit_discount/100))


    @api.model
    def create(self, vals):
        r = super(bms_service_line, self).create(vals)
        order = self.env['sale.order'].sudo().browse(vals['order_id'])
        product = self.env['product.template'].sudo().browse(vals['product_id'])
        r['specifiedSevices_id'] = order.PrimaryInformation_id
        # ward_id = self.env['res.partner'].search([('id', '=', r['order_partner_id'].id)]).ward_id.name
        # district_id = self.env['res.partner'].search([('id', '=', r['order_partner_id'].id)]).district_id.name
        age =self.env['res.partner'].search([('id', '=', r['order_partner_id'].id)]).age
        # r['district_id'] = district_id
        # r['ward_id'] = ward_id
        r['age'] = age
        if product.do_many_times == False:
            temp = 2
            r['bms_status'] = 3
            collect = r['price_subtotal']
        else:
            temp = 0
            collect = 0


        r['order_line_remedy_id'] = [(0, 0,
                  {
                      'primaryinformation_remedy_id': order.PrimaryInformation_id,
                      'sevices': r['product_id'],
                      'date_start': r['create_date'],
                      'status_do': temp,
                      'collect': collect,
                      'partner_id': r.order_partner_id.id,
                      'doctor':r['doctor'],
                      'doctor_support':r['doctor_sp'],
                      'company_id':r['company_id'],
                  }
                  )]
        return r

    @api.model
    @api.onchange("order_line_remedy_id")
    def onchange_doctor(self):
        for doctor in self.order_line_remedy_id:
            doctor.doctor_start = self.doctor
            if doctor.status_do == 2:
                doctor.dortor_stop = doctor.doctor

        if self.product_id.do_many_times == True:
            for line_remedy in self.order_line_remedy_id:
                total = 0
                befor_compalate = 0
                for money in self.order_line_remedy_id:
                    total = total + money.collect
                    if money.status_do != 2:
                        befor_compalate = befor_compalate + money.collect
                if self.doctor_sp:
                    if line_remedy.status_do == 0:
                        line_remedy.cost_doctor_start = self.product_id.standard_price / 2
                        line_remedy.cost_doctor_support = self.product_id.standard_price / 2
                        line_remedy.money_doctor_start = line_remedy.collect / 2
                        line_remedy.money_doctor_suport = line_remedy.collect / 2

                    if line_remedy.status_do == 1:
                        line_remedy.cost_doctor_start = 0
                        line_remedy.cost_doctor_support = 0
                        line_remedy.money_doctor_start = line_remedy.collect / 2
                        line_remedy.money_doctor_suport = line_remedy.collect / 2
                        line_remedy.money_doctor_stop = total = 0

                    if line_remedy.status_do == 2:
                        line_remedy.cost_doctor_start = self.product_id.standard_price / 3 - self.product_id.standard_price / 2
                        line_remedy.cost_doctor_support = self.product_id.standard_price / 3 - self.product_id.standard_price / 2
                        line_remedy.cost_doctor_stop = self.product_id.standard_price / 3
                        line_remedy.money_doctor_start = total / 3 - befor_compalate / 2
                        line_remedy.money_doctor_suport = total / 3 - befor_compalate /2
                        line_remedy.money_doctor_stop = total / 3
                else:
                    if line_remedy.status_do == 0:
                        line_remedy.cost_doctor_start = self.product_id.standard_price
                        line_remedy.cost_doctor_support = 0
                        line_remedy.money_doctor_start = line_remedy.collect
                        line_remedy.money_doctor_suport = 0

                    if line_remedy.status_do == 1:
                        line_remedy.cost_doctor_start = 0
                        line_remedy.cost_doctor_support = 0
                        line_remedy.money_doctor_start = line_remedy.collect
                        line_remedy.money_doctor_suport = 0

                    if line_remedy.status_do == 2:
                        line_remedy.cost_doctor_start = self.product_id.standard_price / 2 - self.product_id.standard_price
                        line_remedy.cost_doctor_support = 0
                        line_remedy.cost_doctor_stop = self.product_id.standard_price / 2
                        line_remedy.money_doctor_start = total / 2 - befor_compalate
                        line_remedy.money_doctor_suport = 0
                        line_remedy.money_doctor_stop = total / 2

        elif self.product_id.do_many_times == False:
            if len(self.order_line_remedy_id) != 1:
                raise Warning('Không tạo thêm liệu trình cho dịch vụ 1 lần!')
            for item in self.order_line_remedy_id:
                item.doctor_start = self.doctor
                if self.doctor_sp != 'hr.employee()':
                    item.money_doctor_start = item.collect /2
                    item.money_doctor_suport = item.collect /2
                    item.cost_doctor_start = self.product_id.standard_price /2
                    item.cost_doctor_support = self.product_id.standard_price /2
                if self.doctor_sp == 'hr.employee()':
                    item.money_doctor_start = item.collect
                    item.money_doctor_suport = 0
                    item.cost_doctor_start = self.product_id.standard_price
                    item.cost_doctor_support = 0


class bms_service_perform(models.Model):
    _name = "clinic.perform.service"

    name = fields.Date(string='Ngày')
    service = fields.Many2one('product.template', string='Dịch vụ')
    doctor = fields.Many2one('hr.employee', string='Bác sĩ', default=lambda self: self.sudo().env.uid)
    assistant = fields.Many2one('hr.employee', string='Trợ thủ',default=lambda self: self.sudo().env.uid)
    advisory = fields.Many2one('hr.employee', string='Tư vấn',default=lambda self: self.sudo().env.uid)
    payment_amount = fields.Integer(string='Số tiền thanh toán')
    content_results = fields.Text(string='Nội dung kết quả thực hiện')
    status = fields.Selection([('1', 'Tạo mới'), ('2', 'Đang thực hiện'), ('3', 'Kết thức')], 'Trạng thái')
    saleorder_id = fields.Many2one('sale.order', string='Sale Orders')
