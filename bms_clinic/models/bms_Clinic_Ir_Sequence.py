# -*- coding: utf-8 -*-

import datetime
import json
import logging
import random
import select
import threading
import time

import odoo
from odoo import api, fields, models, SUPERUSER_ID
from odoo.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT

_logger = logging.getLogger(__name__)

#ĐỊnh nghĩa các tiền tố"""
SalesOrder = 'PK'
CustomerInvoices = 'HD/%(range_year)s/'
PaymentsCustomerCreditNotesSequence = 'PC.KH/%(range_year)s/'
PaymentsCustomerInvoicesSequence = 'PT.KH/%(range_year)s/'
PaymentsSupplierCreditNotesSequence = 'PT.NCC/%(range_year)s/'
PaymentsSupplierInvoicesSequence = 'PC.NCC/%(range_year)s/'


class bmsClinicIrSequence(models.Model):
    _inherit = 'ir.sequence'

    # bms_prefix = fields.Char(compute='_compute_prefix')
    prefix = fields.Char(compute='_compute_prefix')

    #Kiểm tra các tiền tố"""
    @api.depends('code')
    def _compute_prefix(self):
        for item in self:
            if item.code == 'sale.order':
                item.prefix = SalesOrder
            if item.name == 'Customer Invoices':
                item.prefix = CustomerInvoices
            if item.code == 'account.payment.customer.refund':
                item.prefix = PaymentsCustomerCreditNotesSequence
            if item.code == 'account.payment.customer.invoice':
                item.prefix = PaymentsCustomerInvoicesSequence
            if item.code == 'account.payment.supplier.refund':
                item.prefix = PaymentsSupplierCreditNotesSequence
            if item.code == 'account.payment.supplier.invoice':
                item.prefix = PaymentsSupplierInvoicesSequence




