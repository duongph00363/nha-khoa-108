# -*- coding: utf-8 -*-

from odoo import fields, models, api
from datetime import date

#Liệu trình điều trị
class BmsClinicRemedy(models.Model):
    _name = 'bms.clinic.remedy'
    _description = 'Liệu trình điều trị'
    _order = "id desc"

    partner_id = fields.Many2one('res.partner', string='Khách hàng', index=True)
    company_id = fields.Many2one('res.company', string="Công ty", default=lambda self: self.env.user.company_id)
    code_remedy = fields.Char("Mã liệu trình", compute='_compute_code_remedy')
    date_start = fields.Date(string='Ngày thực hiện', default=lambda self: fields.datetime.now())
    sevices = fields.Many2one('product.product', string='Dịch vụ',required=True)
    doctor = fields.Many2one('hr.employee', string='Bác sỹ')
    doctor_support = fields.Many2one(related="primaryinformation_remedy_id._sale_order_ids.order_line.doctor_sp", string='Bấc sĩ tư vấn')
    content_result = fields.Text(string=u'Nội dung và kết quả thực hiện')
    status_do = fields.Selection([(0, 'Tạo mới'), (1, 'Đang thực hiện'), (2, 'Hoàn tất')], string=u'Trạng thái', store=True)
    primaryinformation_remedy_id = fields.Many2one('bms.clinic.primaryinformation', string='Hồ sơ khám và điều trị')
    # primaryinformation_remedy_id = fields.Many2one(related='so_line_ids.specifiedSevices_id')
    code_brief = fields.Char(related="primaryinformation_remedy_id.profileCode",string="Mã hồ sơ")
    status_id = fields.Char("Giai đoạn liệu trinh")
    doctor_start = fields.Many2one("hr.employee", string="Bác sỹ điều trị lần đầu")
    dortor_stop = fields.Many2one("hr.employee", string="Bác sỹ điều trị lần cuối")
    doctor_suport_money = fields.Char("Bác sỹ tư vấn")
    money_doctor_start = fields.Float("Doanh số bác sỹ đầu")
    money_doctor_stop = fields.Float("Doanh số bác sỹ cuối")
    money_doctor_suport = fields.Float("Doanh số bác sỹ tư vấn")
    cost_doctor_start = fields.Float("Chi phí xưởng người điều trị đầu")
    cost_doctor_support = fields.Float("Chi phí xưởng cho người hỗ trợ")
    cost_doctor_stop = fields.Float("Chi phí xưởng cho người điều trị cuối")
    collect = fields.Float('Thu', required=True,
                           help="Số tiền thu từ khách hàng từng lần đến khám, số liệu này được dùng cho báo cáo Lương", default=0.0)
    so_line_ids = fields.Many2one('sale.order.line')
    @api.multi
    def name_get(self):
        res = []
        for record in self:
            name = record.sevices.name
            res.append((record.id, name))
        return res

    @api.multi
    def _compute_code_remedy(self):
        for item in self:
            item.code_remedy = 'LT' + str(item.id)