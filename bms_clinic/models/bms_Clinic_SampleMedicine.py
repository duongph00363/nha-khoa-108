# -*- coding: utf-8 -*-

from odoo import fields, models, api

class BmsClinicSampleMedicine(models.Model):
    _name = 'bms.clinic.samplemedicine'
    _description = 'Đơn thuốc mẫu'
    _order = "id desc"

    name = fields.Char(string="Tên đơn thuốc", required=True)
    advice_id = fields.Many2one('bms.clinic.advice', string='Lời dặn')
    prescription_details_ids = fields.One2many('bms.clinic.use', 'samplemedicine_id', string='Chi tiết đơn thuốc',required=True)
    active = fields.Boolean(string='Trạng thái', default=True)
    company_id = fields.Many2one('res.company', string="Công ty", default=lambda self: self.env.user.company_id)
