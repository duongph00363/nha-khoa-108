# -*- coding: utf-8 -*-

from odoo import fields, models, api
from odoo.exceptions import Warning
import datetime

class bms_result(models.Model):
	_name = "clinic.result"

	date = fields.Date("Ngày in")
	Doctor_id = fields.Many2one("hr.employee", string="Bác sĩ",default=lambda self: self.sudo().env.uid)
	sevices = fields.Many2one("product.template", "Dịch vụ")
	result = fields.Text("Kết quả")
	image = fields.Many2many("ir.attachment", string="Hình ảnh cận lâm sàng")
	conclusion = fields.Text(string="Kết luận")
	saleorder_id = fields.Many2one("sale.order", string="saleorder")
	company_id = fields.Many2one('res.company', string="Công ty", default=lambda self: self.env.user.company_id)

	@api.multi
	@api.onchange("date")
	def _setup_date(self):
		if self.date:
			self.date = datetime.date.today()
