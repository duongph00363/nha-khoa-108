# -*- coding: utf-8 -*-

from odoo import fields, models,api

#khanh.pg 08.07.2018 task: 55 sản phẩm dịch vụ
class BmsClinicProductSevices(models.Model):
    _inherit = 'product.template'
    _description = 'sản phẩm dịch vụ'

    #mã dịch vụ
    service_code = fields.Char(string='Mã dịch vụ', readonly=True, compute='_compute_service_code')
    drug_code = fields.Char(string='Mã thuốc')
    CLS_results = fields.Boolean(string='Trả kết quả cận lâm sàng', default=False)
    do_many_times = fields.Boolean(string='Thực hiện nhiều lần', default=False)
    stop = fields.Boolean(string='Trạng thái tạm ngưng', default=False)
    unit_product = fields.Many2one('product.uom', string='Đơn vị sử dụng')
    check_tooth = fields.Boolean(string='Dịch vụ lấy cảo răng ?', default=False)

    type = fields.Selection([
        ('service', 'Dịch vụ'),
        ('drug', 'Thuốc')], string='Loại sản phẩm',default='service', required=True,
        help='A stockable product is a product for which you manage stock. The "Inventory" app has to be installed.\n'
             'A consumable product, on the other hand, is a product for which stock is not managed.\n'
             'A service is a non-material product you provide.\n'
             'A digital content is a non-material product you sell online. The files attached to the products are the one that are sold on '
             'the e-commerce such as e-books, music, pictures,... The "Digital Product" module has to be installed.')

    @api.multi
    def _compute_service_code(self):
        for item in self:
            item.service_code = 'SP' + str(item.id)

    def _get_default_uom_id(self):
        if self.type == 'drug':
            res = self.env["product.uom"].search([('name', 'like', 'Viên')], limit=1).id
        else:
            res = self.env["product.uom"].search([('name', 'like', 'Lần')], limit=1).id
        return res

    uom_id = fields.Many2one(
        'product.uom', 'Unit of Measure',
        default=_get_default_uom_id, required=False,
        help="Đơn vị đo lường mặc định được sử dụng cho.")

