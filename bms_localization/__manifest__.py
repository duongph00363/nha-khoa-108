# -*- coding: utf-8 -*-
{
    'name': 'BMS Localization',
    'version': '10.0.1.0.0',
    'author': 'BMS Group Global',
    'license': 'AGPL-3',
    'summary': 'BMS guest stay',
    'sequence': 30,
    'category': 'BMS Module',
    'website': 'http://bmsgroupglobal.com/',
    'images': [],
    'depends': ['base', 'crm'],
    'data': [
        'security/bms_localization_server_group.xml',
        'security/ir.model.access.csv',
        'views/localization_view.xml',
        'data/localization_data.xml',
        'views/bms_res_partner.xml',
        'views/bms_view_company_form.xml',
    ],
    'demo': [
    ],
    'qweb': [
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}