# -*- coding: utf-8 -*-
from odoo import fields, models, api


class bms_res_company(models.Model):
	_inherit = 'res.company'
	_description = 'Res company'

	district_id = fields.Many2one('res.country.district', string='District')
	ward_id = fields.Many2one('res.country.ward', string='Ward')

	@api.onchange('state_id')
	def _onchange_state_id(self):
		if self.state_id:
			return {'domain': {'district_id': [('state_id', '=', self.state_id.id)]}}
		else:
			return {'domain': {'district_id': []}}

	@api.onchange('district_id')
	def _onchange_district_id(self):
		if self.district_id:
			return {'domain': {'ward_id': [('district_id', '=', self.district_id.id)]}}
		else:
			return {'domain': {'ward_id': []}}