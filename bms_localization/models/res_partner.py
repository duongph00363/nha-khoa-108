# -*- coding: utf-8 -*-
from odoo import fields, models, api


class ResPartner(models.Model):
    _inherit = 'res.partner'
    _description = 'Res partner'

    @api.model
    def _get_country_id(self):
        return self.env.user.company_id.country_id

    @api.model
    def _get_state_id(self):
        return self.env.user.company_id.state_id

    @api.model
    def _get_district_id_id(self):
        return self.env.user.company_id.district_id

    @api.model
    def _get_ward_id_id(self):
        return self.env.user.company_id.ward_id

    district_id = fields.Many2one('res.country.district', string='District', default=_get_district_id_id)
    ward_id = fields.Many2one('res.country.ward', string='Ward',default=_get_ward_id_id)
    country_id = fields.Many2one('res.country', string='Country', ondelete='restrict', default=_get_country_id)
    state_id = fields.Many2one("res.country.state", string='State', ondelete='restrict',default=_get_state_id)


    @api.onchange('state_id')
    def _onchange_state_id(self):
        if self.state_id:
            return {'domain': {'district_id': [('state_id', '=', self.state_id.id)]}}
        else:
            return {'domain': {'district_id': []}}

    @api.onchange('district_id')
    def _onchange_district_id(self):
        if self.district_id:
            return {'domain': {'ward_id': [('district_id', '=', self.district_id.id)]}}
        else:
            return {'domain': {'ward_id': []}}