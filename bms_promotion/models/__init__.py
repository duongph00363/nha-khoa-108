# -*- coding: utf-8 -*-
from . import bms_primary_information
from . import bms_product_pricelist
from . import bms_member_card
from . import bms_save_point
from . import bms_accumulate_point
from . import bms_respartner
from . import bms_invoice
from . import bms_country_group
from . import bms_General_setting
from . import bms_sale_oder
from . import bms_payments