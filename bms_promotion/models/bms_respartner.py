# -*- coding: utf-8 -*-

from odoo import fields, models, api


class bms_promotion_res_partner(models.Model):
	_inherit = "res.partner"

	barcode = fields.Many2one('bms.member.card')
	country_group_id = fields.Many2one(comodel_name="res.country.group")
	group_level_card = fields.Many2many(comodel_name="res.country.group", string="Nhóm")

	@api.model
	def create(self, vals):
		record = super(bms_promotion_res_partner, self).create(vals)
		if "phone" in vals:
			data = {'user': record.id, 'phone': vals['phone']}

		else:
			data = {'user': record.id}

		self.env['bms.accumulate.point'].sudo().create(data)

		return record

	# button the thanh vien
	@api.multi
	def action_membercard(self):
		abc = self.env['bms.accumulate.point'].search([('user','=',self.id)])
		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'bms.accumulate.point',
			'type': 'ir.actions.act_window',
			'res_id': abc.id,
		}

