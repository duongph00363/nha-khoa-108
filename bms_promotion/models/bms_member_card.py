# -*- coding: utf-8 -*-
from odoo import fields, models, api
import datetime
from odoo.exceptions import Warning
import re


class bms_member_card(models.Model):
	_name = "bms.member.card"
	_description = "the thanh vien"

	user = fields.Many2one("res.partner", required=True, string="Tên khách hàng", help="abc xyz")
	name = fields.Char(string="Mã thẻ thành viên", required=True, help="abc xyz")
	phone = fields.Char(string="Số điện thoại", related="user.phone", readonly=True, store=True, help="abc xyz")
	date_start = fields.Date(string="Ngày mở thẻ", required=True, default=datetime.date.today(), help="abc xyz")
	card = fields.Many2one("res.country.group", string="Hạng thẻ", help="abc xyz", required=True)
	# name_card = fields.Char(related='card.name')
	point_gift = fields.Integer("diem tang ban dau")
	active = fields.Boolean(default=True)
	state = fields.Selection([
		('draft', 'Nháp'),
		('confirm', 'Xác nhận'),
		('cancel', 'Hủy'),
	], string='Status', readonly=True, copy=False, index=True, default='draft', help="abc xyz")
	save_point_ids = fields.One2many("bms.save.point", "member_card_id", "diem tich")
	accumulate_point_ids = fields.One2many("bms.accumulate.point", "barcode", "lich su tich diem")
	# card_id =fields.Many2one("res.country.group", string="Hạng thẻ", required=True, compute='_check_card',help="abc xyz")
	active_readonly = fields.Boolean(default=False)
	# for_card = fields.Many2one("res.country.group", string="Hạng thẻ", required=True, help="abc xyz",)

	_sql_constraints = [
		('name_unique', 'UNIQUE (name)', 'Mã Thẻ đã bị trùng')
	]

	# action button xac nhan
	@api.one
	@api.multi
	def action_confirm(self):
		val_barcode = self.env['bms.accumulate.point'].search([('user', '=', self.user.id)])
		val_barcode['barcode'] = self.id
		point = self.env['bms.card.level'].search_read([('product_pricelst_id', '=', self.card.id)])
		for po in point:
			self.point_gift = po['start_point']
		if self.phone:
			data = {'name': self.user.id, 'phone': self.phone,
					'source': self.name, 'point': self.point_gift,
					'barcode': self.name,
					'time': self.date_start, 'content': 'Tặng điểm tạo thẻ' + ' ' + self.card.name}
		else:
			data = {'name': self.user.id,
					'source': self.name, 'point': self.point_gift,
					'barcode': self.name,
					'time': self.date_start, 'content': 'Tặng điểm tạo thẻ' + ' ' + self.card.name}
		a = self.env['bms.save.point'].create(data)
		for rec in self:
			rec.state = 'confirm'
		act = self.env['bms.member.card'].search([('user', '=', self.user.id)])
		for act_id in act:
			if self.id != act_id['id']:
				act_id['active'] = False

		respartner = self.env['res.partner'].search([('id', '=', self.user.id)])
		val = []
		for res in respartner['group_level_card']:
			if res.group_card == True:
				val.append(([3,res.id]))
			else:
				val.append(res.id)
		if self.card.id not in val:
			val.append(self.card.id)
		respartner['group_level_card'] = val
		return a


	@api.model
	def create(self, vals):
		record = super(bms_member_card, self).create(vals)
		val_barcode = self.env['bms.accumulate.point'].search([('user', '=', record.user.id)])
		val_barcode['barcode'] = record.id
		point = self.env['bms.card.level'].search_read([('product_pricelst_id', '=', record.card.id)])
		for po in point:
			record.point_gift = po['start_point']
		if record.phone:
			data = {'name': record.user.id, 'phone': record.phone,
					'source': record.name, 'point': record.point_gift,
					'barcode': record.name,
					'time': record.date_start, 'content': 'Tặng điểm tạo thẻ' + ' ' + record.card.name}
		else:
			data = {'name': record.user.id,
					'source': record.name, 'point': record.point_gift,
					'barcode': record.name,
					'time': record.date_start, 'content': 'Tặng điểm tạo thẻ' + ' ' + record.card.name}
		self.env['bms.save.point'].create(data)
		for rec in record:
			rec.state = 'confirm'
		act = self.env['bms.member.card'].search([('user', '=', record.user.id)])
		for act_id in act:
			if record.id != act_id['id']:
				act_id['active'] = False

		respartner = self.env['res.partner'].search([('id', '=', record.user.id)])
		val = []
		for res in respartner['group_level_card']:
			if res.group_card == True:
				val.append(([3, res.id]))
			else:
				val.append(res.id)
		if record.card.id not in val:
			val.append(record.card.id)
		respartner['group_level_card'] = val

		# self.env['bms.accumulate.point'].sudo().write(data)
		record.user.barcode = record.id
		return record

	@api.multi
	def action_cancel(self):
		for rec in self:
			rec.state = 'cancel'

	@api.multi
	def action_draft(self):
		for rec in self:
			rec.state = 'draft'

	# check xem duoc o nhom the nao
	@api.multi
	@api.onchange('user')
	def _for_card(self):
		accumulate_point = self.env['bms.accumulate.point'].search_read([('user', '=', self.user.id)])
		val_total_invoice = 0
		val_card_level_id = None
		for rec in accumulate_point:
			val_total_invoice = rec['total_invoice']
		card_level = self.env['bms.card.level'].search_read()
		for rec_card_level in card_level:
			if val_total_invoice > rec_card_level['amount_from'] and val_total_invoice < rec_card_level['amount_to']:
				val_card_level_id = (rec_card_level['product_pricelst_id'][0])
		if val_card_level_id:
			self.card = val_card_level_id
	#

class bms_card_level(models.Model):
	_name = "bms.card.level"

	name = fields.Char("Thành viên thân thiết")
	amount_from = fields.Float("Định mức tiền từ", default=0)
	amount_to = fields.Float("Định mức tiền đến", default=1)
	start_point = fields.Float("Điểm tặng bắt đầu")
	min_point = fields.Float("Điểm tối thiểu")
	product_pricelst_id = fields.Many2one("res.country.group", "Nhóm khách hàng",required=True)

	@api.multi
	@api.onchange("amount_to")
	def _check(self):
		if self.amount_to <= self.amount_from:
			raise Warning('Định mức tiền đến phải lớn hơn định mức tiền từ')


class bms_card_upgrade(models.Model):
	_name = 'bms.card.upgrade'

	user = fields.Many2one("res.partner", required=True, string="Tên khách hàng")
	phone = fields.Char(string="Số điện thoại", related="user.phone", readonly=True, store=True, help="abc xyz")
	date_start = fields.Date(string="Ngày mở thẻ", required=True, default=datetime.date.today(), help="abc xyz")
	card = fields.Many2one("res.country.group", string="Hạng thẻ", required=True, help="abc xyz")
	# name_card = fields.Char(related='card.name')
	barcode = fields.Char('ma the')
	state = fields.Boolean(default=False, store=True)

	@api.multi
	def action_upgrade(self):
		compose_form = self.env.ref('bms_member_card_view_form', False)
		ctx = dict(
			default_model='bms.card.upgrade',
			default_user=self.user.id,
			default_card=self.card.id,
			default_name=self.barcode,
			default_active_readonly=True,

		)
		return {
			# 'name': _('Compose Email'),
			'type': 'ir.actions.act_window',
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'bms.member.card',
			'views': [(compose_form, 'form')],
			'view_id': compose_form,
			'target': 'new',
			'context': ctx,
		}
