# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
import datetime


class bms_payments(models.Model):
	_inherit = "account.payment"

	check_card = fields.Boolean("Mang Thẻ")
	val_barcode = fields.Many2one('bms.member.card', string='Mã thẻ', store=True)
	value_point = fields.Float('tich diem')
	payments_line_ids = fields.One2many('bms.account.payment.line', 'payments_id', 'phieu kham')
	amount = fields.Monetary(string='Payment Amount', required=True)
	partner_id_card = fields.Many2one("res.partner", required=True, string="Tên khách hàng", related="val_barcode.user")
	primaryinformation_id = fields.Many2one('bms.clinic.primaryinformation', 'ho so kham benh')
	level_card = fields.Many2one('res.country.group', string='Hạng thẻ',related="val_barcode.card")
	upgrade_level_card = fields.Many2one('res.country.group', string='Hạng the nang cap', compute='compute_level_car')

	@api.model
	def create(self, vals):
		recode = super(bms_payments, self).create(vals)
		if recode.check_card == True:
			if recode.val_barcode:
				value = self.env['product.pricelist'].search([('country_group_ids', '=', recode.level_card.id)])
				for item in value.item_ids[0]:
					if item.point_save == 'fixed':
						value_point = item.fixed_point
					elif item.point_save == 'percentage':
						value_point = recode.amount * float(item.percent_point) / 100
						# value_point = recode.amount * float(value['item_ids'].percent_point) / 100
					data = {'name': recode.partner_id_card.id, 'phone': recode.partner_id.phone,
							'source': recode.name, 'point': value_point,
							'barcode': recode.val_barcode.name,
							'time': datetime.date.today(),
							'content': 'Tích điểm thanh toán' + ' ' + recode.name}
					self.env['bms.save.point'].create(data)
		return recode

	@api.one
	@api.depends('check_card', 'val_barcode')
	def compute_level_car(self):
		accumulate_point = self.env['bms.accumulate.point'].search_read([('user', '=', self.partner_id_card.id)])
		val_total_invoice = 0
		val_card_level_id = None
		barcode_name = None
		val_card_defaul = None
		for rec in accumulate_point:
			if rec['total_invoice']:
				val_total_invoice = rec['total_invoice']
			if rec['barcode']:
				barcode_name = rec['barcode'][1]
			if rec['card_level_id']:
				val_card_defaul = rec['card_level_id'][0]
		card_level = self.env['bms.card.level'].search_read()
		for rec_card_level in card_level:
			if val_total_invoice > rec_card_level['amount_from'] and val_total_invoice < rec_card_level[
				'amount_to']:
				val_card_level_id = (rec_card_level['product_pricelst_id'][0])
		if val_card_level_id == None or val_card_level_id == val_card_defaul:
			state = False
		else:
			self.upgrade_level_card = val_card_level_id

	@api.multi
	def upgrade_card_id(self):
		compose_form = self.env.ref('bms_member_card_view_form', False)
		ctx = dict(
			default_model='bms.card.upgrade',
			default_user=self.partner_id_card.id,
			default_card=self.upgrade_level_card.id,
			default_name=self.val_barcode.name,
			default_active_readonly=True,
		)
		return {
			# 'name': _('Compose Email'),
			'type': 'ir.actions.act_window',
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'bms.member.card',
			'views': [(compose_form, 'form')],
			'view_id': compose_form,
			'target': 'new',
			'context': ctx,
		}


	@api.onchange('payments_line_ids')
	def _amount_all(self):
		for order in self:
			payments_total = 0.0
			for line in order.payments_line_ids:
				payments_total += line.amount_payments
			order.update({
				'amount': payments_total,
			})


	@api.multi
	def action_upgrade_card(self):
		accumulate_point = self.env['bms.accumulate.point'].search_read([('user', '=', self.partner_id.id)])
		val_total_invoice = 0
		val_card_level_id = None
		barcode_name = None
		val_card_defaul = None
		for rec in accumulate_point:
			if rec['total_invoice']:
				val_total_invoice = rec['total_invoice']
			if rec['barcode']:
				barcode_name = rec['barcode'][1]
			if rec['card_level_id']:
				val_card_defaul = rec['card_level_id'][0]
		card_level = self.env['bms.card.level'].search_read()
		for rec_card_level in card_level:
			if val_total_invoice > rec_card_level['amount_from'] and val_total_invoice < rec_card_level['amount_to']:
				val_card_level_id = (rec_card_level['product_pricelst_id'][0])
		if val_card_level_id == None or val_card_level_id == val_card_defaul:
			state = False
		else:
			state = True
		compose_form = self.env.ref('bms_card_upgrade_view_act', False)
		ctx = dict(
				default_model='account.payment',
				default_user=self.partner_id.id,
				default_card=val_card_level_id,
				default_state=state,
				default_barcode=barcode_name,
			)
		return {
			'type': 'ir.actions.act_window',
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'bms.card.upgrade',
			'views': [(compose_form, 'form')],
			'view_id': compose_form,
			'target': 'new',
			'context': ctx,
		}


class bms_payments_line(models.Model):
	_name = "bms.account.payment.line"

	partne_id = fields.Many2one('res.partner', 'Tên khách hàng', related="name.partner_id")
	name = fields.Many2one('sale.order', 'Mã Phiếu khám')
	barcode = fields.Many2one('bms.member.card', 'Mã thẻ', related="name.barcode", readonly=True, store=True)
	check = fields.Boolean('mượn thẻ', related="name.check", readonly=True, store=True, default=False)
	val_barcode = fields.Many2one('bms.member.card', 'Mã thẻ mượn', related="name.val_barcode", readonly=True,
								  store=True)
	amount_payments = fields.Float('Tiền tạm ứng', required=True, default=0)
	payments_id = fields.Many2one('account.payment')
	amount_payments = fields.Float('Tiền tạm ứng', required=True)

	payments_id = fields.Many2one('account.payment', 'Phiếu tạm ứng')
	pricelist_id = fields.Many2one('product.pricelist', string='Bảng giá', related="name.pricelist_id", readonly=True,
								   store=True, default=1)