# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class ResConfigSettings(models.TransientModel):
    _inherit = "res.config.settings"
    _description = "Cau Hinh Chung"

    rule_use_card = fields.Integer()

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        params = self.env['ir.config_parameter'].sudo()
        res.update(rule_use_card=int(params.get_param('bms_promotion.rule_use_card')))
        return res


    def set_values(self):
        super(ResConfigSettings, self).set_values()
        Param = self.env['ir.config_parameter'].sudo()
        Param.set_param("bms_promotion.rule_use_card", self.rule_use_card)

