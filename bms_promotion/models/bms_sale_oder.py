# -*- coding: utf-8 -*-

from odoo import api, exceptions, fields, models, _
from odoo.exceptions import Warning
import datetime

import re


class bms_sale_oder(models.Model):
    _inherit = "sale.order"

    barcode = fields.Many2one('bms.member.card', 'Thẻ thành viên')
    check = fields.Boolean('Mượn thẻ', default=False)
    val_barcode = fields.Many2one('bms.member.card', 'Thẻ mượn')
    point_ttv = fields.Float('Điểm thẻ thành viên', default=0, store=True, compute='_calculation')
    point_ttv_act = fields.Float('Điểm khả dụng', default=0, store=True, compute='_calculation')
    point_use = fields.Float('SD điểm TTV', default=0)
    draft_amount = fields.Monetary('tien quy doi point', compute='_calculation', store=True)
    all_amount = fields.Monetary('tong so tien can thanh toan', compute='_amount_all', store=True)
    val_partner_id = fields.Many2one('res.partner', string='partner_id for val_barcode')
    card_level_id = fields.Many2one(comodel_name="res.country.group", compute='_calculation', string="Hạng thẻ")
    cancellation_of_service = fields.Monetary(string='Giảm trừ do hủy dịch vụ', compute='compute_cancellation_of_service')

    @api.multi
    def compute_cancellation_of_service(self):
        cancellation_of_service_1 = 0
        for item in self:
            for items in item.order_line:
                cancellation_of_service_1 += items.total_money_collected
            item.cancellation_of_service = item.amount_total - cancellation_of_service_1


    @api.depends('order_line.price_total', 'draft_amount')
    def _amount_all(self):
        for order in self:
            amount_untaxed = amount_tax = 0.0
            for line in order.order_line:
                amount_untaxed += line.price_subtotal
                amount_tax += line.price_tax
            order.update({
                'amount_untaxed': order.pricelist_id.currency_id.round(amount_untaxed),
                'amount_tax': order.pricelist_id.currency_id.round(amount_tax),
                'amount_total': amount_untaxed + amount_tax - order.draft_amount,
                'all_amount': amount_untaxed + amount_tax,
                'draft_amount': order.draft_amount,
            })


    @api.one
    @api.depends('check', 'point_ttv_act', 'point_ttv', 'barcode', 'check', 'val_barcode', 'point_use', 'partner_id')
    def _calculation(self):

        if self.check == False:
            self.point_ttv = 0
            self.point_ttv_act = 0
            if self.partner_id:
                val = self.env['bms.accumulate.point'].search([('user', '=', self.partner_id.id)])
                if val['barcode']:
                    self.barcode = val['barcode']
                    self.card_level_id = val['card_level_id']
                    self.point_ttv = val['point_ttv']
                    if val['point_availability'] < 0:
                        self.point_ttv_act = 0
                    else:
                        self.point_ttv_act = val['point_availability']
        else:
            if self.val_barcode:
                val = self.env['bms.accumulate.point'].search([])
                for code in val:
                    if self.val_barcode in code['barcode']:
                        self.card_level_id = code['card_level_id']
                        self.point_ttv = code['point_ttv']
                        self.val_partner_id = code['user']
                        if code['point_availability'] < 0:
                            self.point_ttv_act = 0
                        else:
                            self.point_ttv_act = code['point_availability']
        for rec in self:
            rec.update({'point_ttv': rec.point_ttv,
                        'point_ttv_act': rec.point_ttv_act,
                        })

        # diem quy doi

        self.draft_amount = self.point_use * 1


    @api.onchange('point_use', 'partner_id')
    def _onchange_point_use(self):
        if self.point_use < 0:
            raise Warning('Điểm sử dụng phải lớn hơn 0')
        else:
            if self.point_use > self.point_ttv_act:
                raise Warning('Điểm sử dụng không được vượt quá điểm khả dụng')

    @api.multi
    def action_confirm(self):
        self._action_confirm()
        if self.env['ir.config_parameter'].sudo().get_param('sale.auto_done_setting'):
            self.action_done()
        if self.point_use != 0:
            if self.check == False:
                if self.barcode:
                    val_partner = self.partner_id.id
                    val_source = self.barcode.name
                    val_phone = self.partner_id.phone
                    val_content = 'Trừ điểm thanh toán đơn hàng' + ' ' + self.name
                else:
                    val_partner = self.partner_id.id
                    val_phone = self.partner_id.phone
                    val_source = self.barcode.name
                    val_content = 'Trừ điểm thanh toán đơn hàng' + ' ' + self.name

            else:
                val_partner = self.val_partner_id.id
                val_source = self.val_barcode.name
                val_phone = self.val_partner_id.phone
                val_content = 'Trừ điểm cho mượn thẻ tại đơn hàng' + ' ' + self.name
            data = {'name': val_partner, 'phone': val_phone,
                    'source': val_source, 'point': self.point_use * -1,
                    'barcode': self.barcode.name,
                    'time': datetime.date.today(),
                    'content': val_content}
            self.env['bms.save.point'].create(data)
        return True

    # vals bang gia
    @api.multi
    @api.onchange('partner_id')
    def onchange_partner_id(self):
        """
        Update the following fields when the partner is changed:
        - Pricelist
        - Payment terms
        - Invoice address
        - Delivery address
        """
        if not self.partner_id:
            self.update({
                'partner_invoice_id': False,
                'partner_shipping_id': False,
                'payment_term_id': False,
                'fiscal_position_id': False,
            })
            return

        addr = self.partner_id.address_get(['delivery', 'invoice'])
        if self.card_level_id:
            product_pricelist = self.env['product.pricelist'].search(
                [('country_group_ids', '=', self.card_level_id.id)])
            if product_pricelist:
                for id_pricelist in product_pricelist:
                    id_list = id_pricelist.id
                values = {
                    'pricelist_id': id_list,
                    'payment_term_id': self.partner_id.property_payment_term_id and self.partner_id.property_payment_term_id.id or False,
                    'partner_invoice_id': addr['invoice'],
                    'partner_shipping_id': addr['delivery'],
                    'user_id': self.partner_id.user_id.id or self.env.uid
                }
            else:
                values = {
                    'pricelist_id': self.partner_id.property_product_pricelist and self.partner_id.property_product_pricelist.id or False,
                    'payment_term_id': self.partner_id.property_payment_term_id and self.partner_id.property_payment_term_id.id or False,
                    'partner_invoice_id': addr['invoice'],
                    'partner_shipping_id': addr['delivery'],
                    'user_id': self.partner_id.user_id.id or self.env.uid
                }
        else:
            values = {
                'pricelist_id': self.partner_id.property_product_pricelist and self.partner_id.property_product_pricelist.id or False,
                'payment_term_id': self.partner_id.property_payment_term_id and self.partner_id.property_payment_term_id.id or False,
                'partner_invoice_id': addr['invoice'],
                'partner_shipping_id': addr['delivery'],
                'user_id': self.partner_id.user_id.id or self.env.uid
            }

        if self.env['ir.config_parameter'].sudo().get_param(
                'sale.use_sale_note') and self.env.user.company_id.sale_note:
            values['note'] = self.with_context(lang=self.partner_id.lang).env.user.company_id.sale_note

        if self.partner_id.team_id:
            values['team_id'] = self.partner_id.team_id.id
        self.update(values)
