# -*- coding: utf-8 -*-

from itertools import chain

from odoo import api, fields, models, tools, _


class bms_pricelist(models.Model):
	_inherit = "product.pricelist"

	amount_from = fields.Float("Định mức tiền từ")
	amount_to = fields.Float("Định mức tiền đến")
	start_point = fields.Float("Điểm tặng bắt đầu")
	min_point = fields.Float("Điểm tặng tối thiểu")
	status = fields.Selection([
		('1', 'Thẻ thành viên'),
		('2', 'Chương trình khuyến mãi')], string='Status', default='1')


class bms_pricelist(models.Model):
	_inherit = "product.pricelist.item"

	point_save = fields.Selection([
		('fixed', 'Điểm cố định'),
		('percentage', 'Phần trăm'), ], index=True, default='fixed')
	fixed_point = fields.Float("fix point")
	percent_point = fields.Float("Percentage Price")
	point = fields.Char("Tích điểm")

	@api.onchange('fixed_point', 'percent_point', 'point')
	def _get_point_save(self):

		if self.point_save == 'fixed':
			self.point = _("%s Điểm") % (self.fixed_point)
		elif self.point_save == 'percentage':
			self.point = _("%s %% Thanh toán") % (self.percent_point)
