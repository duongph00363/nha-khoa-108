# -*- coding: utf-8 -*-
from odoo import fields, models, api
import datetime

class bms_save_point(models.Model):
	_name = "bms.save.point"
	_description = "chi tiet tich diem"

	name = fields.Many2one("res.partner",required=True, string="Tên khách hàng")
	content = fields.Char("Thanh toán dịch vụ",required=True)
	phone = fields.Char(string="Số điện thoại", related="name.phone", store=True)
	time = fields.Datetime(string="Thời gian", default= datetime.datetime.today(),required=True)
	source = fields.Char("Nguồn",required=True)
	point = fields.Float("Điểm",required=True)
	active = fields.Boolean(default=True)
	state = fields.Selection([
		('draft', 'Nháp'),
		('confirm', 'Xác nhận'),
		('cancel', 'Hủy'),
	], string='Status', readonly=True, copy=False, index=True, default='draft')
	member_card_id = fields.Many2one("bms.member.card","the")
	accumulate_point_id = fields.Many2one(comodel_name="bms.accumulate.point")
	barcode = fields.Char("Mã Thẻ")

	@api.multi
	@api.onchange("time")
	def _setup_date(self):
		if self.time:
			self.time = datetime.datetime.today()

	@api.multi
	def action_confirm(self,vals):
		for rec in self:
			rec.state = 'confirm'

	@api.multi
	def action_cancel(self):
		for rec in self:
			rec.state = 'cancel'

	@api.multi
	def action_draft(self):
		for rec in self:
			rec.state = 'draft'