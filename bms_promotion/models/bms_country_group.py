# -*- coding: utf-8 -*-

from odoo import api, exceptions, fields, models


class bms_res_country_group(models.Model):
	_inherit = "res.country.group"

	group_card = fields.Boolean("Nhóm theo hạng thẻ")
	name_card_id = fields.Many2one("bms.card.level","Quy tắc hạng hạng thẻ")
	partner_ids = fields.One2many(comodel_name="res.partner",inverse_name="country_group_id",compute="_compute_partner", string="Danh sách khách hàng")

	_sql_constraints = [
		('name_unique', 'UNIQUE (name)', 'Nhóm khách hàng đã tồn tại!')
	]


	@api.one
	@api.depends('partner_ids')
	def _compute_partner(self):
		id_parner = []
		name_group = self.env['res.partner'].search([('group_level_card', '=', self.id)])
		for id in name_group:
			id_parner.append(id.id)
		self.partner_ids = self.env['res.partner'].browse(id_parner)