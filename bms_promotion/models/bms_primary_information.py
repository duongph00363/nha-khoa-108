# -*- coding: utf-8 -*-

from odoo import api, exceptions, fields, models
import datetime


class bms_primaryinformation(models.Model):
	_inherit = "bms.clinic.primaryinformation"

	payments_ids = fields.One2many('account.payment', 'primaryinformation_id', 'ho so tam ung')

	# cash_payment_amount = fields.Monetary(string="Tiền mặt", compute='_compute_payback_amount',
	#                                       store=True, help='Tính toán số tiền phải trả lại cho khách hàng.')
	check_card = fields.Boolean("Mang Thẻ")
	val_barcode = fields.Many2one('bms.member.card', string='Mã thẻ', store=True, default=lambda self: self.env['bms.member.card'].search([('user','=',self.name.id)]))




	@api.onchange('name')
	def compute_val_barcode(self):
		self.val_barcode = self.env['bms.member.card'].search([('user','=',self.name.id)]).id

	# button tạm ứng
	@api.multi
	def open_form(self):
		check_card = False
		card_barcode = None
		journal_cash_id = self.env['account.journal'].sudo().search([('type', '=', 'cash'),
																	 ('company_id', '=', self.company_id.id)])
		compose_form = self.env.ref('account.payment.form', False)

		ctx = dict(
			default_model='bms.clinic.primaryinformation',
			default_partner_type='customer',
			default_partner_id=self.name.id,
			# default_payments_line_ids=val_id,
			default_payment_type='inbound',
			# default_amount=self.cash_payment_amount,
			default_journal_id=journal_cash_id.id,
			default_company_id=self.company_id.id,
			default_profileCoce_primaryInformation=self.id,
			default_check_card=check_card,
			default_val_barcode=card_barcode,
			default_bms_payment_reasons=self.profileCode,
			default_check_readonly=True,
			default_amount=self.totalAmountBePaid,
			default_bms_info=self.totalAmountBePaid,
		)
		return {
			'type': 'ir.actions.act_window',
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'account.payment',
			'views': [(compose_form, 'form')],
			'view_id': compose_form,
			'target': 'current',
			'context': ctx,
		}



	@api.multi
	@api.onchange('_sale_order_ids','check_card')
	def _card(self):
		if self._sale_order_ids:
			if self._sale_order_ids[0].barcode:
				self.val_barcode = self._sale_order_ids[0].barcode.id
			elif self._sale_order_ids[0].check == True and self._sale_order_ids[0].val_barcode:
				self.val_barcode = self._sale_order_ids[0].val_barcode.id