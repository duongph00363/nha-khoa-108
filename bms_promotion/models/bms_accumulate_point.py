# -*- coding: utf-8 -*-
from odoo import api, exceptions, fields, models, _


class bms_accumulate_point(models.Model):
	_name = "bms.accumulate.point"
	_description = "lich su tich diem"

	name = fields.Char(related='user.name')
	user = fields.Many2one("res.partner", string="Tên khách hàng", readonly=True, help="abc xyz")
	phone = fields.Char(string="Số điện thoại", related="user.phone", store=True, readonly=True)
	barcode = fields.Many2one(comodel_name="bms.member.card", string="Mã thẻ", store=True)
	# card_level_id = fields.Many2one(comodel_name="product.pricelist",compute='compute_cardlevel', string="Hạng thẻ", readonly=True)
	card_level_id = fields.Many2one(comodel_name="res.country.group", compute='compute_cardlevel', string="Hạng thẻ",
									readonly=True)
	total_invoice = fields.Float("Tổng tiền", compute="compute_total_invoice", readonly=True)
	debt_total = fields.Integer("Tổng tiền nợ", compute="compute_total_invoice", readonly=True)
	point_ttv = fields.Float("Điểm thẻ thành viên", compute="_compute_point", readonly=True)
	point_availability = fields.Float(string="Điểm khả dụng", compute="_compute_point", readonly=True)
	save_point_ids = fields.One2many("bms.save.point", "accumulate_point_id", compute="_compute_save_point",
									 string="Lịch sử tích điểm", readonly=True)

	# member_card_id = fields.Many2one("bms.member.card", "the thanh vien")

	# @api.depends('user')
	# def compute_barcode(self):
	# 	for rec in self:
	# 		rec.barcode = rec.user.barcode

	@api.one
	@api.depends('user', 'card_level_id',)
	def compute_cardlevel(self):
		card = self.env['bms.member.card'].search_read([('user', '=', self.user.id)])
		for val in card:
			self.card_level_id = val['card']
		# self.barcode = val['id']

	@api.one
	@api.depends('user', 'total_invoice', 'debt_total', )
	def compute_total_invoice(self):
		invoice_ids = self.env['bms.clinic.primaryinformation'].search([('name', '=', self.user.id)])

		val_total = 0
		val_debt = 0
		for invoicing in invoice_ids:
			val_total = val_total + invoicing['all_total_money']
		self.total_invoice = val_total
		payment_ids = self.env['account.payment'].search([('partner_id', '=', self.user.id)])
		for payment in payment_ids:
			val_debt = val_debt + payment.amount

		if self.total_invoice - val_debt < 0:
			self.debt_total = 0
		else:
			self.debt_total = self.total_invoice - val_debt

	@api.one
	@api.depends('user', 'point_ttv', 'point_availability', 'card_level_id')
	def _compute_point(self):
		point_ids = self.env['bms.save.point'].search_read([('name', '=', self.user.id)])
		val_ttv = 0
		for val in point_ids:
			val_ttv = val_ttv + val['point']
		self.point_ttv = val_ttv
		min_point_ids = self.env['bms.card.level'].search_read([('product_pricelst_id', '=', self.card_level_id.name)])
		min_val = 0
		for min in min_point_ids:
			min_val = min['min_point']
			if self.point_ttv - min_val < 0:
				self.point_availability = 0
			else:
				self.point_availability = self.point_ttv - min_val


	@api.one
	@api.depends('save_point_ids')
	def _compute_save_point(self):
		save_point = self.env['bms.save.point'].search_read([('name', '=', self.user.id)])
		ids = []
		for id_save in save_point:
			ids.append(id_save['id'])
		self.save_point_ids = self.env['bms.save.point'].browse(ids)

	@api.multi
	def action_upgrade(self):
		accumulate_point = self.env['bms.accumulate.point'].search_read([('user', '=', self.user.id)])
		val_total_invoice = 0
		val_card_level_id = None
		barcode_name = None
		val_card_defaul = None
		for rec in accumulate_point:
			if rec['total_invoice']:
				val_total_invoice = rec['total_invoice']
			if rec['barcode']:
				barcode_name = rec['barcode'][1]
			if rec['card_level_id']:
				val_card_defaul = rec['card_level_id'][0]
		card_level = self.env['bms.card.level'].search_read()
		for rec_card_level in card_level:
			if val_total_invoice > rec_card_level['amount_from'] and val_total_invoice < rec_card_level['amount_to']:
				if rec_card_level['product_pricelst_id'][0]:
					val_card_level_id = (rec_card_level['product_pricelst_id'][0])
		if val_card_level_id == None or val_card_level_id == val_card_defaul:
			state = False
		else:
			state = True
		compose_form = self.env.ref('bms_card_upgrade_view_act', False)
		ctx = dict(
			default_model='account.payment',
			default_user=self.user.id,
			default_card=val_card_level_id,
			default_state=state,
			default_barcode=barcode_name,
		)
		return {
			# 'name': _('Compose Email'),
			'type': 'ir.actions.act_window',
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'bms.card.upgrade',
			'views': [(compose_form, 'form')],
			'view_id': compose_form,
			'target': 'new',
			'context': ctx,
		}
