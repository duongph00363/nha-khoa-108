# -*- coding: utf-8 -*-
{
	'name': "bms_promotion",

	'summary': """
        Module quản lý phòng khám của BMS TECHNOLOGY""",

	'description': """
        Module quản lý phòng khám của BMS TECHNOLOGY
    """,

	'author': "BMS TECHNOLOGY",
	'website': "http://www.bmstech.io",
	# support for show when filter App
	'installable': True,
	'application': True,
	'category': 'BMS Clinic',
	'version': '0.1',

	# any module necessary for this one to work correctly
	'depends': ['base', 'hr', 'crm', 'sale','account_invoicing','bms_clinic'],

	# always loaded
	'data': [
		'security/bms_promotion_server_group.xml',
		'security/ir.model.access.csv',
		'views/bms_membercard.xml',
		'views/bms_save_point.xml',
		'views/bms_accumulate_point.xml',
		'views/bms_product_pricelist.xml',
		'views/bms_pricelist_item.xml',
		'views/bms_card_level.xml',
		'views/bms_view_country_group.xml',
		'views/bms_res_partner.xml',
		'views/bms_primary_information.xml',
		# 'views/bms_Generalsetting.xml',
		'views/bms_sale_oder.xml',
		'views/bms_payments.xml',
		'views/bms_payments_line.xml',
		'views/bms_view_account_payment_invoice_form.xml',
		'views/bms_card_upgrade.xml',
		'views/bms_account_invoice_form.xml',
		'views/menu.xml',
	],
	# only loaded in demonstration mode
	'demo': [

	],
}
