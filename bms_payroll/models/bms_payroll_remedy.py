# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import date


class BmsPayrollEmployees(models.Model):
	_inherit = 'bms.clinic.remedy'

	collect = fields.Float('Thu', required=True, help="Số tiền thu từ khách hàng từng lần đến khám, số liệu này được dùng cho báo cáo Lương")
