# -*- coding: utf-8 -*-
from odoo import fields, models, api
import datetime

class bms_hrpaysliprunworkdayline(models.Model):
	_name = "hr.payslip.run.work.line"

	hr_employee_id = fields.Many2one('hr.employee','Nhân viên')
	cost_amount = fields.Float('Doanh số')
	cost = fields.Float('Chi phí xưởng')
	salary = fields.Float('Lương cứng', related="hr_employee_id.cash_base")
	count_tartar = fields.Float('Số lần lấy cao răng')
	hr_payslip_run_id = fields.Many2one('hr.payslip.run')
