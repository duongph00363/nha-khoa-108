# -*- coding: utf-8 -*-
from odoo import fields, models, api
import datetime

class bms_hrpaysliprunworkdayline(models.Model):
	_name = "hr.payslip.run.workday.line"

	hr_employee_id = fields.Many2one('hr.employee','Nhân viên')
	date_regulations = fields.Float('Ngày quy định', default=0)
	date_reality = fields.Float('Ngày thực tế',default=0)
	overtime = fields.Float ('Làm thêm',default=0)
	day_off = fields.Float('Ngày nghỉ',default=0)
	hr_payslip_run_id = fields.Many2one(comodel_name='hr.payslip.run')

	@api.multi
	@api.onchange('date_regulations','date_reality','overtime','day_off')
	def compute_date(self):
		if self.date_reality - self.date_regulations <0:
			self.overtime = 0
		else:
			self.overtime = self.date_reality - self.date_regulations
		if self.date_regulations - self.date_reality<0:
			self.day_off = 0
		else:
			self.day_off = self.date_regulations - self.date_reality
