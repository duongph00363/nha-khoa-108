# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import date

class BmsPayrollEmployees(models.Model):
    _inherit = 'hr.employee'

    payroll_structure_id = fields.Many2one('hr.payroll.structure','Cấu trúc lương')