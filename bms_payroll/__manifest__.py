{
	'name': "bms_payroll",

	'summary': """
        Module quản lý phòng khám của BMS TECHNOLOGY""",

	'description': """
        Module quản lý phòng khám của BMS TECHNOLOGY
    """,

	'author': "BMS TECHNOLOGY",
	'website': "http://www.bmstech.io",
	# support for show when filter App
	'installable': True,
	'application': True,
	'category': 'BMS Clinic',
	'version': '0.1',

	# any module necessary for this one to work correctly
	'depends': ['base', 'hr', 'crm', 'sale','account_invoicing','bms_clinic','hr_payroll'],

	# always loaded
	'data': [
		'views/bms_payroll_employees.xml',
		# 'views/bms_payroll_primary_information.xml',
		'views/bms_hr_payslip_run.xml',
		'views/bms_hr_payslip_run_workday_line.xml',
		'views/bms_hr_payslip_form.xml',

	],
	# only loaded in demonstration mode
	'demo': [


	],
}
