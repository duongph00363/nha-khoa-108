{
	'name': "pivot_inventory",

	'summary': """
        Module quản lý phòng khám của BMS TECHNOLOGY""",

	'description': """
        Module quản lý phòng khám của BMS TECHNOLOGY
    """,

	'author': "BMS TECHNOLOGY",
	'website': "http://www.bmstech.io",
	# support for show when filter App
	'installable': True,
	'application': True,
	'category': 'BMS Clinic',
	'version': '0.1',

	# any module necessary for this one to work correctly
	'depends': ['stock',
],

	# always loaded
	'data': [
		'views/bms_stock_quant_pivot.xml',
	],
	# only loaded in demonstration mode
	'demo': [

	],
}
# -*- coding: utf-8 -*-
