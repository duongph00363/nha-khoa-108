# -*- coding: utf-8 -*-

from odoo import fields, models, api
from unidecode import unidecode
from odoo.exceptions import UserError
from datetime import datetime, timedelta
import math


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    auto_birthday_send_time = fields.Float(string = 'Happy Birthday auto send SMS time')

    auto_dating_send_time = fields.Float(string = 'Remind dating send SMS time')
    auto_dating_send_date_before = fields.Char(string = 'Remind dating send SMS date')

    is_auto_birthday = fields.Boolean(string = 'Happy Birthday auto send SMS')
    is_new_member_card = fields.Boolean(string = 'New member card SMS')
    is_changing_member_card = fields.Boolean(string = 'Changing member card SMS')
    is_remind_dating = fields.Boolean(string = 'Remind dating SMS')

    auto_birthday_content = fields.Text(string = 'Happy Birthday SMS content')
    new_member_card_content = fields.Text(string = 'New member card SMS content')
    changing_member_card_content = fields.Text(string = 'Changing member card SMS content')
    remind_dating_content = fields.Text(string = 'Remind dating SMS content')

    auto_birthday_accent = fields.Boolean(string = 'Accent', default = True)
    new_member_card_accent = fields.Boolean(string = 'Accent', default = True)
    changing_member_card_accent = fields.Boolean(string = 'Accent', default = True)
    remind_dating_accent = fields.Boolean(string = 'Accent', default = True)

    auto_birthday_count = fields.Integer(string = 'Count', compute = '_compute_number_of_sms_birthday')
    new_member_card_count = fields.Integer(string = 'Count', compute = '_compute_number_of_sms_member_card')
    changing_member_card_count = fields.Integer(string = 'Count',
                                                compute = '_compute_number_of_sms_changing_member_card')
    remind_dating_count = fields.Integer(string = 'Count', compute = '_compute_number_of_sms_remind_dating')

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        params = self.env['ir.config_parameter'].sudo()
        res.update(
            auto_birthday_send_time = params.get_param('at_auto_sms.auto_birthday_send_time'),
            auto_dating_send_time = params.get_param('at_auto_sms.auto_dating_send_time'),
            auto_dating_send_date_before = params.get_param('at_auto_sms.auto_dating_send_date_before'),

            is_auto_birthday = params.get_param('at_auto_sms.is_auto_birthday'),
            is_new_member_card = params.get_param('at_auto_sms.is_new_member_card'),
            is_changing_member_card = params.get_param('at_auto_sms.is_changing_member_card'),
            is_remind_dating = params.get_param('at_auto_sms.is_remind_dating'),

            auto_birthday_content = params.get_param('at_auto_sms.auto_birthday_content'),
            new_member_card_content = params.get_param('at_auto_sms.new_member_card_content'),
            changing_member_card_content = params.get_param('at_auto_sms.changing_member_card_content'),
            remind_dating_content = params.get_param('at_auto_sms.remind_dating_content'),

            auto_birthday_accent = params.get_param('at_auto_sms.auto_birthday_accent'),
            new_member_card_accent = params.get_param('at_auto_sms.new_member_card_accent'),
            changing_member_card_accent = params.get_param('at_auto_sms.changing_member_card_accent'),
            remind_dating_accent = params.get_param('at_auto_sms.remind_dating_accent'),

            auto_birthday_count = int(params.get_param('at_auto_sms.auto_birthday_count')),
            new_member_card_count = int(params.get_param('at_auto_sms.new_member_card_count')),
            changing_member_card_count = int(params.get_param('at_auto_sms.changing_member_card_count')),
            remind_dating_count = int(params.get_param('at_auto_sms.remind_dating_count')),
        )
        return res

    def set_values(self):
        super(ResConfigSettings, self).set_values()
        param = self.env['ir.config_parameter'].sudo()
        param.set_param('at_auto_sms.auto_birthday_send_time', self.auto_birthday_send_time)
        param.set_param('at_auto_sms.auto_dating_send_time', self.auto_dating_send_time)
        param.set_param('at_auto_sms.auto_dating_send_date_before', self.auto_dating_send_date_before)

        param.set_param('at_auto_sms.is_auto_birthday', self.is_auto_birthday)
        param.set_param('at_auto_sms.is_new_member_card', self.is_new_member_card)
        param.set_param('at_auto_sms.is_changing_member_card', self.is_changing_member_card)
        param.set_param('at_auto_sms.is_remind_dating', self.is_remind_dating)

        param.set_param('at_auto_sms.auto_birthday_content', self.auto_birthday_content)
        param.set_param('at_auto_sms.new_member_card_content', self.new_member_card_content)
        param.set_param('at_auto_sms.changing_member_card_content', self.changing_member_card_content)
        param.set_param('at_auto_sms.remind_dating_content', self.remind_dating_content)

        param.set_param('at_auto_sms.auto_birthday_accent', self.auto_birthday_accent)
        param.set_param('at_auto_sms.new_member_card_accent', self.new_member_card_accent)
        param.set_param('at_auto_sms.changing_member_card_accent', self.changing_member_card_accent)
        param.set_param('at_auto_sms.remind_dating_accent', self.remind_dating_accent)

        param.set_param('at_auto_sms.auto_birthday_count', self.auto_birthday_count)
        param.set_param('at_auto_sms.new_member_card_count', self.new_member_card_count)
        param.set_param('at_auto_sms.changing_member_card_count', self.changing_member_card_count)
        param.set_param('at_auto_sms.remind_dating_count', self.remind_dating_count)

        try:
            int(self.auto_dating_send_date_before)
        except Exception as e:
            print(e, type(e))
            raise UserError('Trước ngày hẹn phải là số')

        self._compute_auto_birthday_send_time()
        self._compute_remind_dating_send_time()

    @api.depends('auto_birthday_send_time')
    def _compute_auto_birthday_send_time(self):
        for record in self:
            time = record.auto_birthday_send_time
            string = str(math.floor(time)) + ':' + str(round((time - math.floor(time)) * 60)) + ':00'
            send_time = (datetime.utcnow() + timedelta(days = 0)).strftime('%Y-%m-%d ' + string)
            cron_jobs = self.env['ir.cron'].sudo().search([('name', '=', 'Auto send SMS')])
            for cron in cron_jobs:
                cron.write({'nextcall': send_time})

    @api.depends('auto_dating_send_time')
    def _compute_remind_dating_send_time(self):
        for record in self:
            time = record.auto_dating_send_time
            string = str(math.floor(time)) + ':' + str(round((time - math.floor(time)) * 60)) + ':00'
            send_time = (datetime.utcnow() + timedelta(days = 0)).strftime('%Y-%m-%d ' + string)
            cron_jobs = self.env['ir.cron'].sudo().search([('name', '=', 'Remind dating')])
            for cron in cron_jobs:
                cron.write({'nextcall': send_time})

    @api.depends('auto_birthday_content', 'auto_birthday_accent')
    def _compute_number_of_sms_birthday(self):
        for record in self:
            if not record.auto_birthday_accent:
                if record.auto_birthday_content:
                    sms_character = unidecode(record.auto_birthday_content)
                    if len(sms_character) <= 160:
                        sms_count = 1
                    elif len(sms_character) <= 306:
                        sms_count = 2
                    elif len(sms_character) <= 459:
                        sms_count = 3

                    else:
                        raise UserError('Content is over max character')
                    record.auto_birthday_count = sms_count
                    record.auto_birthday_content = unidecode(record.auto_birthday_content)
            else:
                if record.auto_birthday_content:
                    sms_character = record.auto_birthday_content
                    if len(sms_character) <= 70:
                        sms_count = 1
                    elif len(sms_character) <= 134:
                        sms_count = 2
                    elif len(sms_character) <= 201:
                        sms_count = 3
                    elif len(sms_character) <= 268:
                        sms_count = 4
                    elif len(sms_character) <= 335:
                        sms_count = 5
                    else:
                        raise UserError('Content is over max character')
                    record.auto_birthday_count = sms_count

    @api.depends('new_member_card_content', 'new_member_card_accent')
    def _compute_number_of_sms_member_card(self):
        for record in self:
            if not record.new_member_card_accent:
                if record.new_member_card_content:
                    sms_character = unidecode(record.new_member_card_content)
                    if len(sms_character) <= 160:
                        sms_count = 1
                    elif len(sms_character) <= 306:
                        sms_count = 2
                    elif len(sms_character) <= 459:
                        sms_count = 3
                    else:
                        raise UserError('Content is over max character')
                    record.new_member_card_count = sms_count
                    record.new_member_card_content = unidecode(record.new_member_card_content)
            else:
                if record.new_member_card_content:
                    sms_character = record.new_member_card_content
                    if len(sms_character) <= 70:
                        sms_count = 1
                    elif len(sms_character) <= 134:
                        sms_count = 2
                    elif len(sms_character) <= 201:
                        sms_count = 3
                    elif len(sms_character) <= 268:
                        sms_count = 4
                    elif len(sms_character) <= 335:
                        sms_count = 5
                    else:
                        raise UserError('Content is over max character')
                    record.new_member_card_count = sms_count

    @api.depends('changing_member_card_content', 'changing_member_card_accent')
    def _compute_number_of_sms_changing_member_card(self):
        for record in self:
            if not record.changing_member_card_accent:
                if record.changing_member_card_content:
                    sms_character = unidecode(record.changing_member_card_content)
                    if len(sms_character) <= 160:
                        sms_count = 1
                    elif len(sms_character) <= 306:
                        sms_count = 2
                    elif len(sms_character) <= 459:
                        sms_count = 3

                    else:
                        raise UserError('Content is over max character')
                    record.changing_member_card_count = sms_count
                    record.changing_member_card_content = unidecode(record.changing_member_card_content)
            else:
                if record.changing_member_card_content:
                    sms_character = record.changing_member_card_content
                    if len(sms_character) <= 70:
                        sms_count = 1
                    elif len(sms_character) <= 134:
                        sms_count = 2
                    elif len(sms_character) <= 201:
                        sms_count = 3
                    elif len(sms_character) <= 268:
                        sms_count = 4
                    elif len(sms_character) <= 335:
                        sms_count = 5
                    else:
                        raise UserError('Content is over max character')
                    record.changing_member_card_count = sms_count

    @api.depends('remind_dating_content', 'remind_dating_accent')
    def _compute_number_of_sms_remind_dating(self):
        for record in self:
            if not record.remind_dating_accent:
                if record.remind_dating_content:
                    sms_character = unidecode(record.remind_dating_content)
                    if len(sms_character) <= 160:
                        sms_count = 1
                    elif len(sms_character) <= 306:
                        sms_count = 2
                    elif len(sms_character) <= 459:
                        sms_count = 3

                    else:
                        raise UserError('Content is over max character')
                    record.remind_dating_count = sms_count
                    record.remind_dating_content = unidecode(record.remind_dating_content)
            else:
                if record.remind_dating_content:
                    sms_character = record.remind_dating_content
                    if len(sms_character) <= 70:
                        sms_count = 1
                    elif len(sms_character) <= 134:
                        sms_count = 2
                    elif len(sms_character) <= 201:
                        sms_count = 3
                    elif len(sms_character) <= 268:
                        sms_count = 4
                    elif len(sms_character) <= 335:
                        sms_count = 5
                    else:
                        raise UserError('Content is over max character')
                    record.remind_dating_count = sms_count

    @api.multi
    def auto_send_sms(self):
        config = self.env['res.config.settings'].search([])
        if config:
            is_auto_birthday = config[-1]['is_auto_birthday']
            if is_auto_birthday:
                tag = self.env['at.purpose_tag'].sudo().search([('name', '=', 'CMSN')]).name
                content = config[-1]['auto_birthday_content']
                accented = config[-1]['auto_birthday_accent']
                branches = self.env['res.company'].sudo().search([])
                for branch in branches:
                    brand_names = self.env['at.brand_name'].sudo().search(
                        [('company_id', '=', branch.id), ('purpose_tags', '=', tag)])
                    customers = self.env['res.partner'].sudo().search([('company_id', '=', branch.id)])
                    phone_list = []
                    for cus in customers:
                        phone = cus.phone
                        if phone:
                            if not str(phone).startswith('84'):
                                phone = '84' + phone[1:]
                            phone_list.append(phone)
                    for brand_name in brand_names:
                        data = {
                            'name': 'CMSN ' + str(datetime.today().date()),
                            'brand_name': brand_name.id,
                            'accented': accented,
                            'content': content,
                        }
                        sms = self.env['at.send_sms'].sudo().create(data)
                        sms.send_sms(phone_list)

    @api.multi
    def auto_dating_sms(self):
        config = self.env['res.config.settings'].search([])
        if config:
            is_remind_dating = config[-1]['is_remind_dating']
            if is_remind_dating:
                tag = self.env['at.purpose_tag'].sudo().search([('name', '=', 'CSKH')]).name
                content = config[-1]['remind_dating_content']
                accented = config[-1]['remind_dating_accent']
                branches = self.env['res.company'].sudo().search([])
                for branch in branches:
                    brand_names = self.env['at.brand_name'].sudo().search(
                        [('company_id', '=', branch.id), ('purpose_tags', '=', tag)])

                    events = self.env['calendar.event'].sudo().search([('company_id', '=', branch.id)])
                    for event in events:
                        if event.allday:
                            days = (datetime.strptime(event.start_date, '%Y-%m-%d') - datetime.today()).days + 1
                            dating_time = event.start_date
                        else:
                            days = (datetime.strptime(event.start_datetime,
                                                      '%Y-%m-%d %H:%M:%S') - datetime.today()).days + 1
                            dating_time = event.start_datetime
                        try:
                            if days == int(config[-1]['auto_dating_send_date_before']):
                                phone = event.customer_id.phone
                                if not str(phone).startswith('84'):
                                    phone = '84' + phone[1:]
                                if '$thoigianhen' in content:
                                    content = content.replace('$thoigianhen', str(dating_time))
                                if '$luuy' in content:
                                    content = content.replace('$luuy', str(event.advice))
                                for brand_name in brand_names:
                                    data = {
                                        'name': 'Dating ' + event.customer_id.name,
                                        'brand_name': brand_name.id,
                                        'accented': accented,
                                        'content': content,
                                    }
                                    sms = self.env['at.send_sms'].sudo().create(data)
                                    sms.send_sms(phone)
                        except Exception as e:
                            print(e, type(e))
