# -*- coding: utf-8 -*-

from odoo import fields, models, api
import time
from unidecode import unidecode
from odoo.exceptions import UserError
from datetime import datetime, timedelta
import math


class SMSreport(models.TransientModel):
    _inherit = 'res.config.settings'

    auto_report_send_time = fields.Float(string = 'Send time')
    is_auto_report = fields.Boolean(string = 'report auto send SMS')
    auto_report_content = fields.Text(string = 'report SMS content')
    auto_report_accent = fields.Boolean(string = 'Accent', default = True)
    auto_report_count = fields.Integer(string = 'Count')
    phone = fields.Char(string="Gửi báo cáo cho")
    phone2 = fields.Char()
    phone3 = fields.Char()

    @api.model
    def get_values(self):
        res = super(SMSreport, self).get_values()
        params = self.env['ir.config_parameter'].sudo()
        res.update(
            auto_report_send_time=params.get_param('at_auto_sms.auto_report_send_time'),
            is_auto_report=params.get_param('at_auto_sms.is_auto_report'),
            auto_report_content=params.get_param('at_auto_sms.auto_report_content'),
            auto_report_count=int(params.get_param('at_auto_sms.auto_birthday_count')),
            phone=params.get_param('at_auto_sms.phone'),
            phone2=params.get_param('at_auto_sms.phone2'),
            phone3=params.get_param('at_auto_sms.phone3'),
        )
        return res

    def set_values(self):
        super(SMSreport, self).set_values()
        param = self.env['ir.config_parameter'].sudo()
        param.set_param('at_auto_sms.auto_report_send_time', self.auto_report_send_time)
        param.set_param('at_auto_sms.auto_report_content', self.auto_report_content)
        param.set_param('at_auto_sms.auto_report_accent', self.auto_report_accent)
        param.set_param('at_auto_sms.auto_report_count', self.auto_report_count)
        param.set_param('at_auto_sms.phone', self.phone)
        param.set_param('at_auto_sms.phone', self.phone2)
        param.set_param('at_auto_sms.phone', self.phone3)

        self._compute_auto_report_send_time()

    @api.depends('auto_report_send_time')
    def _compute_auto_report_send_time(self):
        for record in self:
            config = self.env['res.config.settings'].search([])
            time = record.auto_report_send_time
            string = str(math.floor(time)) + ':' + str(round((time - math.floor(time)) * 60)) + ':00'
            send_time = (datetime.utcnow() + timedelta(days=0)).strftime('%Y-%m-%d ' + string)
            cron_jobs = self.env['ir.cron'].sudo().search([('name', '=', 'Report_SMS')])
            for cron in cron_jobs:
                cron.write({'nextcall': send_time})


    @api.multi
    def auto_send_sms_report(self):
        config = self.env['res.config.settings'].search([])
        if config:
           content = config[-1].auto_report_content
           phone_list = []
           if config[-1].phone != False:
               sdt = "84" + config[-1].phone[1:]
               phone_list.append(sdt)
           if config[-1].phone2 != False:
               sdt2 = "84" + config[-1].phone2[1:]
               phone_list.append(sdt2)
           if config[-1].phone3 != False:
               sdt3 = "84" + config[-1].phone3[1:]
               phone_list.append(sdt3)
           company = self.env['res.company'].sudo().search([])
           SMS = ""
           for PK in company:
               clinic_payment = self.env['account.payment'].sudo().search(
                   [('payment_date', '=', datetime.today()), ('company_id', '=', PK.id)])
               HSKH = self.env['bms.clinic.primaryinformation'].sudo().search(
                   [('dateStart', '=', datetime.today()), ('company_id', '=', PK.id)])
               tien = 0.0
               KH = 0
               for clinic in clinic_payment:
                   tien = tien + clinic.amount
               for count in HSKH:
                   KH = KH + 1
               if '$pk-sl-ds' in content:
                   SMS = SMS + str(PK.name) + ' - ' + str(tien).split('.')[0] + "đ" + ' - ' + str(KH) + ". "
               if '$pk-ds' in content:
                   SMS = SMS + str(PK.name) + ' - ' + str(tien).split('.')[0] + "đ" + ". "
           if '$pk-sl-ds' in content:
               content = content.replace('$pk-sl-ds', SMS)
           if '$pk-ds' in content:
               content = content.replace('$pk-ds', SMS)
           HSKH_all = self.env['bms.clinic.primaryinformation'].sudo().search([('dateStart', '=', datetime.today())])
           counts = 0
           for count in HSKH_all:
               counts = counts + 1
           if '$soluongkhachhang' in content:
               content = content.replace('$soluongkhachhang', str(counts))
           payment_all = self.env['account.payment'].sudo().search([('payment_date', '=', datetime.today())])
           total = 0.0
           for tien in payment_all:
               total = total + tien.amount
           if '$tongdoanhso' in content:
               content = content.replace('$tongdoanhso', str(total).split('.')[0] + "đ")
           brand_names = self.env['at.brand_name'].sudo().search([])
           for brand_name in brand_names:
               data = {
                   'name': 'Báo cáo doanh số trong ngày',
                   'brand_name': brand_name.id,
                   'accented': "ilovey",
                   'content': content,
               }
               sms = self.env['at.send_sms'].sudo().create(data)
               for sdt in phone_list:
                   sms.send_sms(sdt)