# -*- coding: utf-8 -*-
from odoo import fields, models, api


class MemberCardPointChangeSMS(models.Model):
    _inherit = 'bms.save.point'

    @api.model
    def create(self, vals):
        save_point = super(MemberCardPointChangeSMS, self).create(vals)
        config = self.env['res.config.settings'].search([])
        if config:
            is_changing_member_card = config[-1]['is_changing_member_card']
            if is_changing_member_card:
                tag = self.env['at.purpose_tag'].sudo().search([('name', '=', 'TTV')]).name
                content = config[-1]['changing_member_card_content']

                if '$mathe' in content:
                    content = content.replace('$mathe', str(save_point.barcode))
                if '$sodiemcong' in content:
                    content = content.replace('$sodiemcong', str(save_point.point))
                accumulate_points = self.env['bms.accumulate.point'].sudo().search([('user', '=', save_point.name.id)])
                for accumulate_point in accumulate_points:
                    if '$diem' in content:
                        content = content.replace('$diem', str(accumulate_point.point_ttv))
                    if '$khadung' in content:
                        content = content.replace('$khadung', str(accumulate_point.point_availability))
                    if '$tongtienthanhtoan' in content:
                        content = content.replace('$tongtienthanhtoan', str(accumulate_point.total_invoice))
                    if '$hangthe' in content:
                        content = content.replace('$hangthe', str(accumulate_point.card_level_id.name))
                accented = config[-1]['changing_member_card_accent']
                branch = save_point.name.company_id
                phone = save_point.phone
                if phone:
                    if not str(phone).startswith('84'):
                        phone = '84' + phone[1:]
                brand_names = self.env['at.brand_name'].sudo().search(
                    [('company_id', '=', branch.id), ('purpose_tags', '=', tag)])
                for brand_name in brand_names:
                    data = {
                        'name': save_point.content,
                        'brand_name': brand_name.id,
                        'accented': accented,
                        'content': content,
                    }
                    sms = self.env['at.send_sms'].sudo().create(data)
                    sms.send_sms(phone)
        return save_point
