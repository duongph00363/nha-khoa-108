# -*- coding: utf-8 -*-
from odoo import fields, models, api


class MemberCardSMS(models.Model):
    _inherit = 'bms.member.card'

    @api.multi
    def action_confirm(self):
        action_confirm = super(MemberCardSMS, self).action_confirm()
        config = self.env['res.config.settings'].search([])
        if config:
            is_new_member_card = config[-1]['is_new_member_card']
            if is_new_member_card:
                tag = self.env['at.purpose_tag'].sudo().search([('name', '=', 'TTV')]).name
                content = config[-1]['new_member_card_content']
                if '$hangthe' in content:
                    content = content.replace('$hangthe', str(self.card.name))
                if '$mathe' in content:
                    content = content.replace('$mathe', str(self.name))
                if '$sodiemcong' in content:
                    content = content.replace('$sodiemcong', str(self.point_gift))
                accumulate_points = self.env['bms.accumulate.point'].sudo().search([('user', '=', self.user.id)])
                for accumulate_point in accumulate_points:
                    if '$diem' in content:
                        content = content.replace('$diem', str(accumulate_point.point_ttv))
                    if '$khadung' in content:
                        content = content.replace('$khadung', str(accumulate_point.point_availability))
                    if '$tongtienthanhtoan' in content:
                        content = content.replace('$tongtienthanhtoan', str(accumulate_point.total_invoice))

                accented = config[-1]['new_member_card_accent']
                branch = self.user.company_id
                phone = self.phone
                if phone:
                    if not str(phone).startswith('84'):
                        phone = '84' + phone[1:]
                brand_names = self.env['at.brand_name'].sudo().search(
                    [('company_id', '=', branch.id), ('purpose_tags', '=', tag)])
                for brand_name in brand_names:
                    data = {
                        'name': 'Tạo thẻ thành viên cho khách hàng ' + self.user.name,
                        'brand_name': brand_name.id,
                        'accented': accented,
                        'content': content,
                    }
                    sms = self.env['at.send_sms'].sudo().create(data)
                    sms.send_sms(phone)
        return action_confirm
