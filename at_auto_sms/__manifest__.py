# -*- coding: utf-8 -*-
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).
{
    "name": 'Automation SMS',
    "summary": 'Auto send SMS to say happy birthday customer',
    "version": "10.0.0.1",
    "category": "BMSAPP",
    "website": "",
    "author": "BMSTECH",
    "license": "LGPL-3",
    "application": True,
    "installable": True,
    "depends": ['base',
                'web',
                'client_sms_brand_name',
                'bms_clinic',
                'bms_promotion',
                ],
    "data": [
        'views/sms_setting_view.xml',
        'views/auto_birthday.xml',
        'views/remind_dating.xml',
        'views/sms_report.xml'
    ],
}
