# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class SendSMSLog(models.Model):
    _name = 'at.send_sms_log'

    name = fields.Char(string=_('Log tittle'), required=True)
    brand_name = fields.Many2one('at.brand_name', string=_('Brand name'), required=True)
    sender = fields.Char(string=_('Sender'), default=lambda self: self.env.user.name, required=True)
    receiver = fields.Char(string=_('Receiver'), required=True)
    send_time = fields.Datetime(string=_('Send time'), required=True)
    content = fields.Text(string=_('SMS content'), required=True)
    state = fields.Selection([('success', _('Success')), ('fail', _('Fail'))],
                             string=_('Status'))
    accented = fields.Boolean(string=_('Accented'))
    send_sms = fields.Many2one('at.send_sms')
