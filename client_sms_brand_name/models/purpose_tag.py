# -*- coding: utf-8 -*-

from odoo import models, fields


class AutoSendSMSHPBD(models.Model):
    _name = 'at.purpose_tag'

    _sql_constraints = [
        ('name', 'UNIQUE (name)', 'The name of the tag must be unique !')
    ]
    name = fields.Char(string='Name')
    brand_name = fields.Many2many('at.brand_name', 'purpose_tags', string="Brandname")