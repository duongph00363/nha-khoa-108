# -*- coding: utf-8 -*-

from odoo import models, fields


class AutoSendSMSHPBD(models.Model):
    _name = 'at.brand_name'

    name = fields.Char(string='Name', required=True)
    company_id = fields.Many2many('res.company', string='Branch', required=True)
    purpose_tags = fields.Many2many('at.purpose_tag', 'brand_name', string='Purpose')