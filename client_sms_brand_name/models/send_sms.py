# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
import datetime
from unidecode import unidecode
from odoo.exceptions import UserError
from odoo.exceptions import Warning
import re
import requests


class SendSMS(models.Model):
    _name = 'at.send_sms'

    name = fields.Char(string = _('SMS Tittle'), required = True)
    brand_name = fields.Many2one('at.brand_name', string = _('Brand name'), required = True)
    accented = fields.Boolean(string = _('Send SMS with accents'), default = False)
    content = fields.Text(string = _('SMS content'), required = True)
    count_sms = fields.Integer(string = _('Number of SMS'), compute = '_compute_number_of_sms')
    send_time = fields.Datetime(string = _('Send time'), default = datetime.datetime.now())
    delivered = fields.Char(string = _('Delivered'))
    state = fields.Selection(
        [('draft', _('Draft')), ('sending', _('Sending')), ('success', _('Success')), ('fail', _('Fail'))],
        string = _('Status'), default = 'draft')
    is_send_all_company = fields.Boolean(string = _('Send all company customers'))
    is_send_all_personal = fields.Boolean(string = _('Send all individual customers'))
    is_send_all_employee = fields.Boolean(string = _('Send all employee'))
    is_send_phone_list = fields.Boolean(string = _('Send attach phone list'))
    send_sms_log = fields.One2many('at.send_sms_log', 'send_sms', string = 'Send SMS log')
    receiver_file_data = fields.Binary()
    receiver_file_name = fields.Char()
    template_file_receiver = fields.Char(
        default = lambda self: self.env['ir.config_parameter'].get_param('web.base.url') +
                               '/client_sms_brand_name/static/template_file/DS.xlsx')

    @api.multi
    def write(self, vals):
        if self.state != 'draft':
            if not (vals.get('state') or vals.get('send_time') or vals.get('delivered')):
                raise UserError(_('Access denied. Only change while status is Draft.'))
        return super(SendSMS, self).write(vals)

    @api.depends('content', 'accented')
    def _compute_number_of_sms(self):
        for record in self:
            if not record.accented:
                if record.content:
                    sms_character = unidecode(record.content)
                    if len(sms_character) <= 160:
                        sms_count = 1
                    elif len(sms_character) <= 306:
                        sms_count = 2
                    elif len(sms_character) <= 459:
                        sms_count = 3
                    # elif len(sms_character) <= 612:
                    #     sms_count = 4
                    else:
                        raise UserError(_('Content is over max character'))
                    record.count_sms = sms_count
            else:
                if record.content:
                    sms_character = record.content
                    if len(sms_character) <= 70:
                        sms_count = 1
                    elif len(sms_character) <= 134:
                        sms_count = 2
                    elif len(sms_character) <= 201:
                        sms_count = 3
                    elif len(sms_character) <= 268:
                        sms_count = 4
                    elif len(sms_character) <= 335:
                        sms_count = 5
                    else:
                        raise UserError(_('Content is over max character'))
                    record.count_sms = sms_count

    def action_send_sms(self):
        list_phone = []
        if not (
                self.is_send_all_company or self.is_send_all_personal or self.is_send_all_employee or self.is_send_phone_list):
            raise UserError(_('Please choice receiver'))
        if self.is_send_phone_list:
            phones = self.get_phone_list_from_attach_file()
            for phone in phones:
                list_phone.append(phone)
        if self.is_send_all_company:
            domain = [('is_company', '=', True)]
            companies = self.env['res.partner'].search(domain)
            for customer in companies:
                phone = customer.mobile
                if phone:
                    if not str(phone).startswith('84'):
                        phone = '84' + phone[1:]
                        list_phone.append(str(phone))
        if self.is_send_all_personal:
            domain = [('is_company', '=', False)]
            customers = self.env['res.partner'].search(domain)
            for customer in customers:
                phone = customer.mobile
                if phone:
                    if not str(phone).startswith('84'):
                        phone = '84' + phone[1:]
                        list_phone.append(str(phone))
        if self.is_send_all_employee:
            employees = self.env['hr.employee'].search([])
            for employee in employees:
                phone = employee.mobile_phone
                if phone:
                    if not str(phone).startswith('84'):
                        phone = '84' + phone[1:]
                        list_phone.append(str(phone))
        self.send_sms(list_phone)

    @api.multi
    def send_sms(self, list_phone):
        for s in self:
            config = self.env['res.config.settings'].search([])
            token = config[-1]['token']
            url = config[-1]['server_name'] + '/api/send_sms'
            if not token:
                raise Warning(_('Connection fail, check your config please!'))
            fail_sms = 0
            if type(list_phone) == list:
                for phone in list_phone:
                    if not s.accented:
                        s.content = unidecode(s.content)
                    data = {
                        'token': token,
                        'title': s.name,
                        'brandname': s.brand_name.name,
                        'accent': s.accented,
                        'content': s.content,
                        'phone': phone
                    }
                    s.write({'send_time': datetime.datetime.utcnow()})
                    s.write({'state': 'sending'})
                    try:
                        send_sms = requests.post(url, data)
                        message = send_sms.json().get('message')
                        if message == 'Send message success.':
                            s.create_send_sms_log(phone, 'success')
                        else:
                            fail_sms += 1
                            s.create_send_sms_log(phone, 'fail')
                    except Exception as e:
                        print('exc')
                        print(type(e))
                        print(e)
                if fail_sms == len(list_phone):
                    s.write({'state': 'fail'})
                else:
                    s.write({'state': 'success'})
                s.write({'delivered': str(str(len(list_phone) - fail_sms) + '/' + str(len(list_phone)))})

            else:
                data = {
                    'token': token,
                    'title': s.name,
                    'brandname': s.brand_name.name,
                    'accent': s.accented,
                    'content': s.content,
                    'phone': list_phone
                }
                try:
                    send_sms = requests.post(url, data)
                    message = send_sms.json().get('message')
                    if message == 'Send message success.':
                        s.write({'state': 'success'})
                        s.write({'delivered': '1/1'})
                        s.create_send_sms_log(list_phone, 'success')
                    else:
                        s.write({'state': 'fail'})
                        s.write({'delivered': '0/1'})
                        s.create_send_sms_log(list_phone, 'fail')
                except Exception as e:
                    print('exc')
                    print(type(e))
                    print(e)

    @api.multi
    def create_send_sms_log(self, phone, state):
        for record in self:
            vals = {
                'name': record.name,
                'brand_name': record.brand_name.id,
                'sender': record.env.user.name,
                'receiver': phone,
                'send_time': record.send_time,
                'accented': record.accented,
                'content': record.content,
                'state': state,
                'send_sms': record.id
            }
        log = record.env['at.send_sms_log'].sudo().create(vals)
        if log:
            return log
        else:
            return False

    @api.multi
    def get_phone_list_from_attach_file(self):
        for record in self:
            excel_data = record.env['read.excel'].read_file(data = record.receiver_file_data, sheet = "Sheet1",
                                                            path = False)
            if excel_data:
                phone_list = []
                try:
                    for column in excel_data[1:]:
                        phone_number = ''.join(re.findall('\d+', column[1]))
                        phone_list.append(phone_number)
                except Exception:
                    raise UserError(_('File format is incorrect'))
                if not phone_list:
                    raise UserError(_('Phone number is must not empty'))

                phone_list_with_code = []
                for phone in phone_list:
                    if not str(phone).startswith('84'):
                        phone = '84' + phone[1:]
                    phone_list_with_code.append(str(phone))
                return phone_list_with_code
            else:
                raise UserError(_('File is incorrect'))
