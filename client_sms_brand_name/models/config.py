# -*- coding: utf-8 -*-

from odoo import fields, models, api
import requests


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    server_name = fields.Char(string = 'Server name')
    username = fields.Char(string = 'Username')
    password = fields.Char(string = 'Password')
    token = fields.Char(string = 'Token', compute='load_token')

    @api.multi
    @api.depends('server_name', 'username', 'password')
    def load_token(self):
        if self.username and self.password and self.server_name:
            data = {
                'username': self.username,
                'password': self.password
            }
            r = requests.post(self.server_name + '/api/auth', data)
            if not r.json().get('token'):
                raise Warning('Connection fail. Check data input again')
            else:
                self.token = r.json().get('token')

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        params = self.env['ir.config_parameter'].sudo()
        res.update(

            server_name = params.get_param('client_sms_brand_name.server_name'),
            username = params.get_param('client_sms_brand_name.username'),
            password = params.get_param('client_sms_brand_name.password'),
            token = params.get_param('client_sms_brand_name.token'),
        )
        return res

    def set_values(self):
        super(ResConfigSettings, self).set_values()
        param = self.env['ir.config_parameter'].sudo()

        param.set_param('client_sms_brand_name.server_name', self.server_name)
        param.set_param('client_sms_brand_name.username', self.username)
        param.set_param('client_sms_brand_name.password', self.password)
        param.set_param('client_sms_brand_name.token', self.token)
