# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError
from unidecode import unidecode


class BMSCompany(models.Model):
    _inherit = 'res.partner'

    title = fields.Char(string=_('SMS Tittle'))
    brand_name = fields.Many2one('at.brand_name', string=_('Brand name'))
    accented = fields.Boolean(string=_('Send SMS with accents'), default=False)
    content = fields.Text(string=_('SMS content'))
    count_sms = fields.Integer(string=_('Number of SMS'), compute='_compute_number_of_sms')

    @api.depends('content', 'accented')
    def _compute_number_of_sms(self):
        for record in self:
            if not record.accented:
                if record.content:
                    sms_character = unidecode(record.content)
                    if len(sms_character) <= 160:
                        sms_count = 1
                    elif len(sms_character) <= 306:
                        sms_count = 2
                    elif len(sms_character) <= 459:
                        sms_count = 3
                    # elif len(sms_character) <= 612:
                    #     sms_count = 4
                    else:
                        raise UserError(_('Content is over max character'))
                    record.count_sms = sms_count
            else:
                if record.content:
                    sms_character = record.content
                    if len(sms_character) <= 70:
                        sms_count = 1
                    elif len(sms_character) <= 134:
                        sms_count = 2
                    elif len(sms_character) <= 201:
                        sms_count = 3
                    elif len(sms_character) <= 268:
                        sms_count = 4
                    elif len(sms_character) <= 335:
                        sms_count = 5
                    else:
                        raise UserError(_('Content is over max character'))
                    record.count_sms = sms_count

    def action_send_sms(self):
        vals = {
            'name': self.title,
            'brand_name': self.brand_name.id,
            'accented': self.accented,
            'content': self.content if self.accented else unidecode(self.content)
        }
        sms = self.env['at.send_sms'].sudo().create(vals)
        phone = self.phone
        if not str(phone).startswith('84'):
            phone = '84' + phone[1:]
        sms.send_sms(phone)
        self.title = ''
        self.brand_name = False
        self.accented = False
        self.content = ''
