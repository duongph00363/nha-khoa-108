# -*- coding: utf-8 -*-
from . import brand_name
from . import read_excel
from . import config
from . import send_sms
from . import send_sms_log
from . import res_partner
from . import hr_employee
from . import purpose_tag