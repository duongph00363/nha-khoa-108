# -*- coding: utf-8 -*-
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).
{
    "name": 'Client SMS Brandname',
    "summary": 'Client module to send SMS with Brandname',
    "version": "10.0.0.1",
    "category": "AlbertTran Addons",
    "website": "",
    "author": "Albert Tran",
    "license": "LGPL-3",
    "application": True,
    "installable": True,
    "depends": ['base',
                'web',
                'hr'
                ],
    "data": [
        'security/at_sms_brandname_client_group.xml',
        'security/at_sms_brandname_client_rule.xml',
        'security/ir.model.access.csv',
        'views/send_sms_view.xml',
        'views/config_view.xml',
        'views/brand_name_view.xml',
        'views/send_sms_log_view.xml',
        'views/hr_employee_view.xml',
        'views/res_partner_view.xml',
        'data/purpose_tag_data.xml',
        'views/menu_view.xml',
    ],
}
